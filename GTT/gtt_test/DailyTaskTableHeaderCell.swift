//
//  DailyTaskTableHeaderCell.swift
//  GTT
//
//  Created by Khor Le Yi on 20/6/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import UIKit

class DailyTaskTableHeaderCell : UITableViewCell {
    
    @IBOutlet weak var MCodeLabel: UILabel!
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var HoursLabel: UILabel!
    

}
