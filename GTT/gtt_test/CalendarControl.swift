//
//  CalendarControl.swift
//  gtt_test
//
//  Created by KHOR, LE YI on 6/9/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import UIKit

@IBDesignable class CalendarControl: UICollectionViewCell {
    
    //MARK: Properties
    var background : UIImage?
    var date : Int?
    var hours : Float?
    
    @IBInspectable var backgroundSize : CGSize = CGSize(width: 50, height: 50){
        didSet{
            setupCalendarCell()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCalendarCell()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupCalendarCell()
    }
    
    //MARK: Private Methods
    private func setupCalendarCell(){
        
        //Background for calendar cell
        
        let bundle = Bundle(for: type(of:self))
        
        let image = UIImage(named: "default", in: bundle, compatibleWith: self.traitCollection)
        
        let imageView = UIImageView(image: image)
        
        imageView.frame = CGRect(x: 0, y: 0, width: backgroundSize.width, height: backgroundSize.height)
        
        addSubview(imageView)
        
        //Date label
        
        let labelView = UILabel(frame:CGRect(x: 0, y: 0, width: 10, height: 10))
        
        labelView.text = String(describing: date) 
        
        addSubview(labelView)
    
    }

}
