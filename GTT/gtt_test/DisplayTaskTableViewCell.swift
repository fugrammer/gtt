//
//  TaskTableViewCell.swift
//  LAL
//
//  Created by Khor Le Yi on 14/6/17.
//  Copyright © 2017 Khor Le Yi. All rights reserved.
//

import UIKit

/// Task class for tableview
class DisplayTaskTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
