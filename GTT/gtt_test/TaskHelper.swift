//
//  ViewController.swift
//  GTT
//
//  Created by Gideon Chia on 6/12/17.
//  Copyright © 2017 Gideon Chia. All rights reserved.
//

import UIKit

class TaskHelper{
    // AppDbLatest variables
    static var gttLatestUpdate = "2017-08-01T00:00:00" // TO REMOVE EVENTUALLY
    // setOpenPeriodDates variables
    static var isOverlappingPeriod = Bool()
    static var openPeriod = [String:Date]()
    // setLocalDB variables
    static var dbLoaded = false
    static var taskDb = [Int:[String:[String:String]]]()
    static var taskHoursDb = [Int:[String:[String:[String:String]]]]()
    static var dayDb = [Int:[String:[String:String]]]()
    static var dateToDayIdDict = [Int:[String:String]]()
    
    /**
     Extracts the month from a Date object.
     - Returns: Single integer from 1 to 12.
     */
    func getMonthFromDate() -> Int {
        let today = Date()
        var calendar = Calendar.init(identifier: .gregorian)
        calendar.timeZone = TimeZone(abbreviation: "GMT")!
        let month = calendar.component(.month, from: today)
        return month
    } // End of getMonthFromDate() function
    
    /**
     Extracts the month from a Date object.
     - Parameters:
     - date: A Date object
     - Returns: Single integer from 1 to 12.
     */
    func getMonthFromDate(date: Date) -> Int {
        let desiredDate = date
        var calendar = Calendar.init(identifier: .gregorian)
        calendar.timeZone = TimeZone(abbreviation: "GMT")!
        let month = calendar.component(.month, from: desiredDate)
        return month
    } // End of getMonthFromDate() function
    
    /**
     Extracts the month from a String object in ISO format.
     - Parameters:
     - date: A String object in the format of "yyyy-MM-dd'T'HH:mm:ss"
     - Returns: Single integer from 1 to 12.
     */    func getMonthFromIsoDate(date: String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation:"GMT")
        let desiredDate = dateFormatter.date(from:date)
        var calendar = Calendar.init(identifier: .gregorian)
        calendar.timeZone = TimeZone(abbreviation: "GMT")!
        let month = calendar.component(.month, from: desiredDate!)
        return month
    } // End of getMonthFromIsoDate function
    
    /**
     Convert a date string object in ISO format to a Date object.
     - Parameters:
     - date: A String object in the format of "yyyy-MM-dd'T'HH:mm:ss"
     - Returns: Date object.
     */
    func convertIsoDateToDate(isoDateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation:"GMT")
        let date = dateFormatter.date(from:isoDateString)!
        var calendar = Calendar.init(identifier: .gregorian)
        calendar.timeZone = TimeZone(abbreviation: "GMT")!
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute ,.second], from: date)
        return (calendar.date(from:components))!
    } // End of convertIsoDateToDate function
    
    /**
     Stores the current time as the latest update made via GTT mobile application.
     */
    func setPhoneLatestUpdate() {
        UserDefaults.standard.set(Date(), forKey:"lastPhoneUpdate")
    } // End of setPhoneLatestUpdate function
    
    /**
     Make an API call to get the latest update made to the GTT database
     - Returns: A Dictionary object.
     */
    func getLatestUpdate() -> [String:Any]{
        // RESPONSE FROM getLatestUpdate API
        
        // TO REPLACE WITH API CALL
        do {
            if let file = Bundle.main.url(forResource: "getLatestUpdate", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    return object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return [:]
    } // End of getLatestUpdate function
    
    /**
     Checks if the database in the phone is the most updated.
     - Returns: true if phone database is the most updated. false if GTT had been updated via web platform.
     */
    func AppDbLatest() -> Bool {
        
        // Get the latest update for the GTT app through API call.
        let response = getLatestUpdate()
        print(response)
        
        // Convert response to Date object for comparison.
        let gttLatestUpdate = convertIsoDateToDate(isoDateString: response["latestUpdate"] as! String)
        print("The latest update on GTT web is \(gttLatestUpdate).")
        
        
        // Get the latest update set by mobile GTT app.
        var lastPhoneUpdate = Date()
        if let date = UserDefaults.standard.object(forKey:"lastPhoneUpdate") as? Date {
            lastPhoneUpdate = date
            print("The latest update on GTT mobile is \(lastPhoneUpdate).")
        }
        
        // Compare both dates and return true if local db has the latest db.
        if gttLatestUpdate != lastPhoneUpdate {
            print("Local database is not synced.")
            return false
        } else {
            print("Local database is synced.")
            return true
        }
    } // End of AppDbLatest function
    
    /**
     Make an API call to get the timetable JSON response from GTT for the current time period and convert it to a Swift Dictionary object.
     - Returns: A Dictionary object.
     */
    func getCurrentTimePeriodTimeTable() -> Any{
        // RESPONSE FROM TIMETABLE API
        
        // TO REPLACE WITH API CALL
        do {
            if let file = Bundle.main.url(forResource: "currentTimeTable", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    return object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return [:]
    } // End of getCurrentTimePeriodTimeTable function
    
    /**
     Make an API call to get the timetable JSON response from GTT for the previous time period and convert it to a Swift Dictionary object.
     - Returns: A Dictionary object.
     */
    func getPreviousTimePeriodTimeTable() -> Any{
        // RESPONSE FROM TIMETABLE
        
        // TO REPLACE WITH API CALL
        do {
            if let file = Bundle.main.url(forResource: "previousTimeTable", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    return object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return [:]
    } // End of getPreviousTimePeriodTimeTable function
    
    /**
     Make an API call to GTT to get the current time period attributes and convert it to a Swift Dictionary object.
     - Returns: A Dictionary object. e.g. "startDate":"2017-06-26T00:00:00"
     */
    func getCurrentTimePeriod() -> [String:Any]{
        // RESPONSE FROM getCurrentTimePeriod
        
        // TO REPLACE WITH API CALL
        do {
            if let file = Bundle.main.url(forResource: "getCurrentTimePeriod", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    return object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return [:]
    } // End of getCurrentTimePeriod function
    
    /**
     Make an API call to GTT to get the previous time period attributes and convert it to a Swift Dictionary object.
     - Returns: A Dictionary object. e.g. "startDate":"2017-06-26T00:00:00"
     */
    func getPreviousTimePeriod() -> [String:Any]{
        // RESPONSE FROM getPreviousTimePeriod
        
        // TO REPLACE WITH API CALL
        do {
            if let file = Bundle.main.url(forResource: "getPreviousTimePeriod", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    return object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return [:]
    } // End of getPreviousTimePeriod function
    
    /**
     Sets the open period dates, i.e. start date and end date, based on whether today's date is in the overlapping period.
     */
    func setOpenPeriodDates(){
        
        // Call getCurrentTimePeriod and getPreviousTimePeriod.
        let previousTimePeriod = getPreviousTimePeriod()
        let currentTimePeriod = getCurrentTimePeriod()
        
        // Create dates for comparison.
        let previousPeriodOpenToDate = convertIsoDateToDate(isoDateString: previousTimePeriod["openTo"] as! String)
        let previousPeriodStartDate = convertIsoDateToDate(isoDateString: previousTimePeriod["startDate"] as! String)
        let currentPeriodStartDate = convertIsoDateToDate(isoDateString: currentTimePeriod["startDate"] as! String)
        let todayDate = Date()
        
        // Set openPeriod start date depending whether today is in overlapping periods.
        if (todayDate <= previousPeriodOpenToDate) {
            TaskHelper.openPeriod["startDate"] = previousPeriodStartDate
            TaskHelper.isOverlappingPeriod = true
            print("Today, \(todayDate) is in over-lapping time periods.")
        }  else {
            TaskHelper.openPeriod["startDate"] = currentPeriodStartDate
            TaskHelper.isOverlappingPeriod = false
            print("Today, \(todayDate) is not in over-lapping time periods.")
        }
        
        // Set openPeriod end date to current time period's end date.
        let currentPeriodEndDate = convertIsoDateToDate(isoDateString: currentTimePeriod["endDate"] as! String)
        TaskHelper.openPeriod["endDate"] = currentPeriodEndDate
        
    } // End of setOpenPeriodDates function
    
    /**
     Checks if there has been a change in the open period dates. e.g. from an overlapping day to a none-overlapping day. This function helps setLocalDB function decide whether to load either one or two months worth of data.
     */
    func openPeriodIsSame() -> Bool {
        if let data = UserDefaults.standard.object(forKey: "lastOpenPeriod") as? Data {
            // If there exists a past open period dates , compare it with the current open period dates.
            if let lastOpenPeriod = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String:Date] {
                if lastOpenPeriod["startDate"] == TaskHelper.openPeriod["startDate"] && lastOpenPeriod["endDate"] == TaskHelper.openPeriod["endDate"] {
                    print("Open dates have not change.")
                    return true
                } else {
                    print("Open dates have change.")
                    return false
                }
            }
        }
        print("No past open dates found.")
        return false
    } // End of openPeriodIsSame function
    
    /**
     Creates all the necessary databases.
     */
    func setLocalDB(){
        // Check if database is loaded.
        if TaskHelper.dbLoaded == true {
            return
        }
        
        setPhoneLatestUpdate() // TO REMOVE EVENTUALLY - Simulates latest phone update as the time when you run the app.
        
        // Get month to create new month database in the local databases.
        var month = Int()
        if getMonthFromDate() == -1 {
            print("Error in getting month")
            return
            // STOP APP.
        } else {
            month = getMonthFromDate()
        }
        
        //  Set the open period based on today's date and determine if current day is in overlapping periods.
        setOpenPeriodDates()
        
        // If there is no change from overlapping day to non-overlapping day and the local db is synced, then load the db as stored in the UserDefaults. Else continue to create a new local database.
        if AppDbLatest() && openPeriodIsSame() {
            print("Loading databases from user defaults..")
            let taskDb  = UserDefaults.standard.object(forKey: "taskDb") as! Data
            let taskHoursDb  = UserDefaults.standard.object(forKey: "taskHoursDb") as! Data
            let dayDb  = UserDefaults.standard.object(forKey: "dayDb") as! Data
            let dateToDayIdDict  = UserDefaults.standard.object(forKey: "dateToDayIdDict") as! Data
            
            TaskHelper.taskDb = NSKeyedUnarchiver.unarchiveObject(with: taskDb) as! [Int : [String : [String : String]]]
            TaskHelper.taskHoursDb = NSKeyedUnarchiver.unarchiveObject(with: taskHoursDb) as! [Int : [String : [String : [String : String]]]]
            TaskHelper.dayDb = NSKeyedUnarchiver.unarchiveObject(with: dayDb) as! [Int : [String : [String : String]]]
            TaskHelper.dateToDayIdDict = NSKeyedUnarchiver.unarchiveObject(with: dateToDayIdDict) as! [Int : [String : String]]
            print("Loading databases from user defaults completed..")
            return
        }
        
        // Initialse variable to track month and data.
        var monthAndData = [Int:Any]()
        
        // Get current time period's timetable.
        let currentTimePeriodTimeTable = getCurrentTimePeriodTimeTable()
        monthAndData[month] = currentTimePeriodTimeTable
        
        // If today is in the overlapping period, get previous time period's timetable.
        if TaskHelper.isOverlappingPeriod {
            let previousTimePeriodTimeTable = getPreviousTimePeriodTimeTable()
            monthAndData[month-1] = previousTimePeriodTimeTable
        }
        
        // Creates local databases based on the required months.
        for (month,timeTableData) in monthAndData{
            print("Fetching latest databases for month \(month)..")
            TaskHelper.taskDb[month] = [:]
            TaskHelper.taskHoursDb[month] = [:]
            TaskHelper.dayDb[month] = [:]
            TaskHelper.dateToDayIdDict[month] = [:]
            
            if let data = timeTableData as? [String:Any] {
                
                //    List of attributes under data["tasks"][#]:
                //        mkvNumber                 //        isActive              //        compound
                //        taskId                    //        taskStatusId          //        target
                //        tradeName                 //        category              //        program
                //        mCode                     //        isApprovalRequired    //        parentCategory
                //        days                      //        executionProject      //        therapeuticArea
                //        canAddTime                //        diseaseArea           //        protocolName
                //        valueStream               //        description           //        canAddTask
                //        targetPhaseDiseaseArea    //        indicationMeDRA       //        protocolNumber
                //        subCategory               //        isDeleted             //        nickName
                //        routeOfAdministration     //        phase                 //        taskName
                //        xCode
                
                if let allTaskData = data["tasks"] as? [Any] {
                    // Iterate through each task and populate taskDb and taskHoursDb with necessary data for the app.
                    for task in allTaskData {
                        
                        let eachTask = task as! [String: Any]
                        if let taskID = eachTask["taskId"] as? Int {
                            
                            // Task DB
                            let taskId = String(describing: taskID)
                            TaskHelper.taskDb[month]![taskId] = [:]
                            
                            // Extract and create attributes for each task
                            TaskHelper.taskDb[month]![taskId]!["taskId"] = taskId
                            
                            if let mCode = eachTask["mCode"] as? String {
                                TaskHelper.taskDb[month]![taskId]!["mCode"] = mCode
                            }
                            if let taskName = eachTask["taskName"] as? String {
                                TaskHelper.taskDb[month]![taskId]!["taskName"] = taskName
                            }
                            
                            // Task Hours DB
                            TaskHelper.taskHoursDb[month]![taskId] = [:]
                            if let eachTaskDayData = eachTask["days"] as? [String:[String:Any]] {
                                
                                for (dayId,value) in eachTaskDayData {
                                    
                                    TaskHelper.taskHoursDb[month]![taskId]![dayId] = [:]
                                    
                                    var hours = String()
                                    if let currentHours = value["hours"] as? Double{
                                        hours = String(currentHours)
                                    } else {
                                        hours = "0.0"
                                    }
                                    
                                    var timeEntityId = String()
                                    if ((value["timeEntityId"] as? Int) != nil) {
                                        timeEntityId = String(describing: value["timeEntityId"])
                                    } else {
                                        timeEntityId = "0"
                                    }
                                    
                                    TaskHelper.taskHoursDb[month]![taskId]![dayId]!["dayId"] = dayId
                                    TaskHelper.taskHoursDb[month]![taskId]![dayId]!["hours"] = hours
                                    TaskHelper.taskHoursDb[month]![taskId]![dayId]!["timeEntityId"] = timeEntityId
                                    
                                } // End For Loop
                            } else {
                                print("Error getting eachTaskDayData")
                            } // End if let eachTaskDayData
                        } // End if let taskID
                    } // End For Loop
                } else {
                    print("Error getting allTaskData")
                }
                
                //    Construct Days Database
                //    List of attributes under response["days"]["#"]:
                //        isOpen                    //        timePeriodId          //        date
                //        isWeekend                 //        dayId
                
                if let allDaysData = data["days"] as? [String:Any] {
                    
                    for (dayId,dayData) in allDaysData {
                        let day = dayData as! [String:Any]
                        TaskHelper.dayDb[month]![dayId] = [:]
                        TaskHelper.dayDb[month]![dayId]!["dayId"] = dayId
                        if let date = day["date"] as? String {
                            TaskHelper.dayDb[month]![dayId]!["date"] = date
                            
                            // Populate dateToDayIdDict
                            let dateOnly = date.components(separatedBy: ("T"))[0]
                            TaskHelper.dateToDayIdDict[month]![dateOnly] = dayId
                            
                        }
                        if let isWeekend = day["isWeekend"] as? Bool {
                            TaskHelper.dayDb[month]![dayId]!["isWeekend"] = String(isWeekend)
                        }
                        if let isOpen = day["isOpen"] as? Bool {
                            TaskHelper.dayDb[month]![dayId]!["isOpen"] = String(isOpen)
                        }
                        if let timePeriodId = day["timePeriodId"] as? Int {
                            TaskHelper.dayDb[month]![dayId]!["timePeriodId"] = String(timePeriodId)
                        }
                        
                    } // End For Loop
                    
                } // End if let allDaysData = data["days]
                
            } // End if let data = timeTableData as? [String:Any]
            
        } // End for (month,timeTableData) in monthAndData
        
        TaskHelper.dbLoaded = true
        
    } // End setLocalDB
    
    
    /**
     Returns all the task IDs for a particular month.
     - Parameters:
     - date: Date Object that contains the month of either current or previous time period.
     - Returns: Array of Task IDs as integers.
     */
    func getAllTaskIds(date: Date) -> Array<Int>{
        let month = getMonthFromDate(date: date)
        guard let monthlyTaskDb = TaskHelper.taskDb[month] else {
            print("getAllTaskIds function - Error when accessing TaskHelper.taskDb[\(month)].")
            return []
        }
        var IDs = [Int]()
        for (taskId,_) in monthlyTaskDb{
            IDs.append(Int(taskId)!)
        }
        IDs.sort()
        return IDs
    } // End of getAllTaskIds function
    
    /**
     Returns all the task m-codes for a particular month.
     - Parameters:
     - date: Date Object that contains the month of either current or previous time period.
     - Returns: Array of task m-codes as integers.
     */
    func getAllMCodes(date: Date) -> Array<String>{
        let month = getMonthFromDate(date: date)
        guard let monthlyTaskDb = TaskHelper.taskDb[month] else {
            print("getAllMCodes function - Error when accessing TaskHelper.taskDb[\(month)].")
            return [""]
        }
        var codes = [String]()
        for (taskId,_) in monthlyTaskDb{
            if let mCode = monthlyTaskDb[taskId]!["mCode"] {
                codes.append(mCode)
            }
        }
        codes.sort()
        return codes
    } // End of getAllMCodes function
    
    /**
     Returns the task data for a particular task.
     - Parameters:
     - date: Date Object that contains the month of either current or previous time period.
     - taskID: Integer representing the task ID.
     - Returns: Dictionary of task attributes and their values.
     */
    func getTask(date: Date, taskID: Int) -> [String:String] {
        let month = getMonthFromDate(date: date)
        guard let task = TaskHelper.taskDb[month]?[String(taskID)] else {
            print("getTask function - Error when accessing TaskHelper.taskDb[\(month)][\(taskID)].")
            return ["":""]
        }
        return task
    } // End of getTask function
    
    /**
     Returns the task m-code for a particular task.
     - Parameters:
     - date: Date Object that contains the month of either current or previous time period.
     - taskID: Integer representing the task ID.
     - Returns: m-code as String.
     */
    func getMCode(date: Date, taskID: Int) -> String {
        let month = getMonthFromDate(date: date)
        guard let mCode = TaskHelper.taskDb[month]?[String(taskID)]?["mCode"] else {
            print("getMCode function - Error when accessing TaskHelper.taskDb[\(month)][\(taskID)]['mCode'].")
            return ""
        }
        return mCode
    } // End of getMCode function
    
    /**
     Returns the task name for a particular task.
     - Parameters:
     - date: Date Object that contains the month of either current or previous time period.
     - taskID: Integer representing the task ID.
     - Returns: task name as String.
     */
    func getTaskName(date: Date, taskID: Int) -> String {
        let month = getMonthFromDate(date: date)
        guard let taskName = TaskHelper.taskDb[month]?[String(taskID)]?["taskName"] else {
            print("getTaskName function - Error when accessing TaskHelper.taskDb[\(month)][\(taskID)]['taskName'].")
            return "No task description."
        }
        return taskName
    } // End of getTaskName function
    
    /**
     Returns the total number of tasks for a particular month.
     - Parameters:
     - date: Date Object that contains the month of either current or previous time period.
     - Returns: m-code as String.
     */
    func getNumberOfTasks(date: Date) -> Int {
        let month = getMonthFromDate(date: date)
        guard let numOfTask = TaskHelper.taskDb[month]?.count else {
            print("getNumberOfTasks function - Error when accessing TaskHelper.taskDb[\(month)].")
            return -1
        }
        return numOfTask
    } // End of getNumberOfTasks function
    
    /**
     Returns the hours entered for each day in a month for a particular task.
     - Parameters:
     - date: Date Object that contains the month of either current or previous time period.
     - taskID: Integer representing the task ID.
     - Returns: String array in hours.
     */
    func getMonthHours(date: Date, taskID: Int) -> Array<String> {
        let month = getMonthFromDate(date: date)
        guard let taskHours = TaskHelper.taskHoursDb[month]?[String(taskID)] else {
            print("getMonthHours function - Unable to retreive hours from TaskHelper.taskDb[\(month)][\(taskID)].")
            return Array(repeating: "0.0", count: 30)
        }
        let numOfDays = taskHours.count
        var hours = Array(repeating: "0.0", count: numOfDays)
        for (day,value) in taskHours {
            if let time = value["hours"] {
                hours[Int(day)!-1] = String(describing: time)
            } else {
                hours[Int(day)!-1] = "0.0"
            }
        }
        return hours
    } // End of getMonthHours function
    
    /**
     Returns the hours entered for a specific day for a particular task.
     - Parameters:
     - date: Date Object that contains the day and month in either current or previous time period.
     - taskID: Integer representing the task ID.
     - Returns: String array in hours.
     */
    func getDateHours (date: Date, taskID: Int) -> String {
        let month = getMonthFromDate(date: date)
        let dayId = String(getDayId(date: date))
        guard let hours = TaskHelper.taskHoursDb[month]?[String(taskID)]?[dayId]?["hours"] else {
            print("getDateHours function - Unable to retreive hours from TaskHelper.taskDb[\(month)][\(taskID)].")
            return "nil"
        }
        return hours
    } // End of getDateHours function
    
    /**
     Sets the hours for a specific day for a particular task.
     - Parameters:
     - date: Date Object that contains the day and month in either current or previous time period.
     - taskID: Integer representing the task ID.
     - hours: Hours to set of Double type.
     */
    func setHours(date: Date, taskID: Int, hours: Double) {
        let month = getMonthFromDate(date: date)
        let dayId = String(getDayId(date: date))
        let hours = String(hours)
        
        // TO REPLACE WITH API CALL TO SET HOURS IN GTT SYSTEM
        
        // TO REPLACE WITH API CALL TO SET LATEST UPDATE FOR GTT SYSTEM
        
        // Set latest update to be in sync with GTT System [NOTE: This time and the above API call should be the same]
        setPhoneLatestUpdate()
        
        // Set local datebase hours
        TaskHelper.taskHoursDb[month]?[String(taskID)]?[dayId]?["hours"] = hours
        
        // Update the datetime to reflect latest update.
        UserDefaults.standard.set(Date(), forKey:"lastPhoneUpdate")
        print("Hours for task \(taskID) on day \(dayId) have been updated to \(hours) hours.")
        
    } // End of setHours function
    
    /**
     Sets the hours for month for a particular task.
     - Parameters:
     - date: Date Object that contains the day and month in either current or previous time period.
     - taskID: Integer representing the task ID.
     - hours: Hours to set of Double type.
     */
    func setHoursArray(days: [MonthlyCalendarDay], taskID: Int) {
     
        // TO REPLACE WITH CODE FOR MONTHLY UPDATE USING ARRAY
        
        for day in days{
            if day.enabled == true{
                self.setHours(date: day.date_object, taskID: taskID, hours: day.hours)
            }
        }
        
    } // End of setHoursArray function

    
    /**
     Gets the start and end dates based on today's date.
     - Returns: Dictionary of the start and end dates
     */
    func getOpenDates() -> [String:Date] {
        return TaskHelper.openPeriod
    } // End of getOpenDates function
    
    /**
     Gets the day ID based on an input date.
     - Parameters:
     - date: Date Object that contains the day and month in either current or previous time period.
     - Returns: Integer representing the day ID of the input date.
     */
    func getDayId(date:Date) -> Int {
        let month = getMonthFromDate(date: date)
        let testDate = String(describing:date).components(separatedBy: (" "))[0]
        guard let dayId = TaskHelper.dateToDayIdDict[month]?[testDate] else {
            print("getDayId function - No day ID found for dateToDayIdDict[\(month)][\(testDate)].")
            return -1
        }
        return Int(dayId)!
    } // End of getDayId function
    
    /**
     Checks if input date is open for editing.
     - Parameters:
     - date: Date Object that contains the day and month in either current or previous time period.
     - Returns: True if editing is allowed, otherwise False.
     */
    func isDayOpen(date: Date)-> Bool {
        // Create date from input
        let dateToCompare = date
        let startDate = TaskHelper.openPeriod["startDate"]
        let endDate = TaskHelper.openPeriod["endDate"]
        
        // Check if today is within open period
        if (dateToCompare >= startDate!) && (dateToCompare <= endDate!){
            return true
        }  else {
            return false
        }
    } // End of isDayOpen function
    
    /**
     Saveds all the local datebases to UserDefaults for subsequent retrieval if necessary when the app is re-opened.
     */
    func saveLocalDbs() {
        // Store all databases and data to UserDefaults
        print("Saving database to UserDefaults.")
        let isOverlappingPeriod = NSKeyedArchiver.archivedData(withRootObject: TaskHelper.isOverlappingPeriod)
        let openPeriod = NSKeyedArchiver.archivedData(withRootObject: TaskHelper.openPeriod)
        let taskDb = NSKeyedArchiver.archivedData(withRootObject: TaskHelper.taskDb)
        let taskHoursDb = NSKeyedArchiver.archivedData(withRootObject: TaskHelper.taskHoursDb)
        let dayDb = NSKeyedArchiver.archivedData(withRootObject: TaskHelper.dayDb)
        let dateToDayIdDict = NSKeyedArchiver.archivedData(withRootObject: TaskHelper.dateToDayIdDict)
        
        UserDefaults.standard.set(isOverlappingPeriod, forKey: "isOverlappingPeriod")
        UserDefaults.standard.set(openPeriod, forKey: "lastOpenPeriod")
        UserDefaults.standard.set(taskDb, forKey: "taskDb")
        UserDefaults.standard.set(taskHoursDb, forKey: "taskHoursDb")
        UserDefaults.standard.set(dayDb, forKey: "dayDb")
        UserDefaults.standard.set(dateToDayIdDict, forKey: "dateToDayIdDict")
        
        print("Database saved in UserDefaults.")
        
    } // End of saveLocalDbs function
    
} // End of class
