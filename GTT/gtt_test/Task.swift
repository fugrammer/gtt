//
//  Task.swift
//  gtt_test
//
//  Created by KHOR, LE YI on 6/14/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import Foundation
import UIKit

/// Class for task. Modify properties according to requirements.
class Task{
    
    //MARK: Properties
    
    var code: Int
    var description: String
    var hours: Double
    
    init?(code: Int, description: String, hours: Double) {
        // The name must not be empty
        guard !description.isEmpty else {
            return nil
        }
        
        // Initialize stored properties.
        self.code = code
        self.description = description
        self.hours = hours
    }

}
