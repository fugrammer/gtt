//
//  LoginViewModel.swift
//  gtt_test
//
//  Created by THAM, THOMAS on 6/30/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import Foundation
import UIKit

let kCornerRadius: CGFloat = 20.0
let kBorderWidth: CGFloat = 1.5

public protocol LoginViewModelDelegate: class {
    func showLoginValidatorAlert(alertMessage: String)
    func loginSuccess()
    func showLogoutAlert()
}

protocol LoginViewModelProtocol {
    
    weak var delegate: LoginViewModelDelegate? {get set}

}

class LoginViewModel : LoginViewModelProtocol {

    weak var delegate: LoginViewModelDelegate?
    
    let profileImageViewBorderWidth: CGFloat = kBorderWidth
    
    //MARK: - Private
    
    //MARK: - Actions
    func didTapNotYouButton() {
        delegate?.showLogoutAlert()
    }
    
    func didTapLoginButton() {
        
    }
}
