//
//  LoginViewController.swift
//  gtt_test
//
//  Created by THAM, THOMAS on 6/30/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit
import LocalAuthentication
import Onboard

/// TouchID or password login for users previously authenticated through OAuth2
class LoginViewController: UIViewController, LoginViewModelDelegate, UITextFieldDelegate {

    //MARK: - Internal
    var loginViewModel: LoginViewModel?

    //MARK: - Outlets
    @IBOutlet weak var loginScrollView: UIScrollView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var profilePicImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfiguration()
        configureNotification()
    }
    

    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.profilePicImageView.image = UIImage.init(data: UserDefaults.standard.object(forKey: "ProfileImage") as! Data)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    func initialConfiguration() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        loginViewModel = LoginViewModel()
        loginViewModel?.delegate = self
        
        guard let loginViewModel = loginViewModel else {
            return
        }
        
        userNameLabel.text = UserDefaults.standard.string(forKey: USER_ISID)

        profilePicImageView.layer.cornerRadius = profilePicImageView.frame.size.width/2
        profilePicImageView.layer.borderWidth = loginViewModel.profileImageViewBorderWidth
        profilePicImageView.layer.borderColor = UIColor.white.cgColor
        profilePicImageView.clipsToBounds = true
        
        profilePicImageView.layer.masksToBounds = true
        profilePicImageView.layer.shadowRadius = 5
        profilePicImageView.layer.shadowOpacity = 0.5
        
        loginButton.layer.cornerRadius = 20
    }
    
    func configureNotification() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        //check for picture update
        if Helper.InternetConnection.isConnectedToInternet() {
            APIHandler().getProfilePicture()
        }
    }
    
    /// Notification triggers when the keyboard pops out
    ///
    /// - parameter notification: NSNotification Object
    func keyboardWillShow(notification: NSNotification) {
        
        guard let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        let insets:UIEdgeInsets = UIEdgeInsetsMake(self.loginScrollView.contentInset.top, 0.0, keyboardSize.height+5, 0.0)
        
        self.loginScrollView.contentInset = insets
        self.loginScrollView.scrollIndicatorInsets = insets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize.height
    }
    
    /// Notification triggers when the keyboard hides in
    ///
    /// - parameter notification: NSNotification Object
    func keyboardWillHide(notification: NSNotification) {
        //  nameOfOutlet.constant = 0.0
        let insets:UIEdgeInsets = UIEdgeInsetsMake(self.loginScrollView.contentInset.top, 0.0, 0.0, 0.0)
        
        self.loginScrollView.contentInset = insets
        self.loginScrollView.scrollIndicatorInsets = insets
    }
    
    /// To make the view (or one of its embedded text fields) to resign the first responder status.
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK: - Actions
    
    @IBAction func didTapNotYouButton(_ sender: Any) {
        loginViewModel?.didTapNotYouButton()
    }
    
    @IBAction func didTapLoginButton(_ sender: Any) {
        authenticateWithTouchID()
    }
    
    func authenticateWithTouchID() {
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            let reason = "Login with Touch ID"
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason, reply: { (success, authError) in
                if success {
                        guard let default_screen = UserDefaults.standard.string(forKey: DEFAULT_SCREEN) else{
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "LoginToParent", sender: nil)
                        }
                        return
                    }
                    if default_screen == DAILY_SCREEN{
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "LoginToParent", sender: nil)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "LoginToParent", sender: nil)
                        }
                    }

                    print("Touch ID Authentication Succeeded")
                }
                else {
                    print("Touch ID Authentication failed")
                    return
                }
            })
        } else {
            print("Touch ID not available")
            return
        }
    }
    
    //MARK: - Login Delegate

    func showLoginValidatorAlert(alertMessage: String) {
        Helper.AlertView.showAlertMessage(viewController: self, alertTitle: ALERT_TITLE, alertMessage: alertMessage)
    }
    
    func loginSuccess() {
        
    }
    
    func showLogoutAlert() {
        let alert = UIAlertController(title: "Alert", message: "Do you want to signout?", preferredStyle: UIAlertControllerStyle.alert)

        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))

        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:  { action in
            self.handleYes()}))
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func handleYes() {
//        UserProfileUtility.sharedInstance.clearProfileData()
        UserDefaults.standard.set(false, forKey: USER_LOGGED_IN)
        showOAuthLoginVC()
    }
    
    func showOAuthLoginVC() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginOAuthViewController = mainStoryboard.instantiateViewController(withIdentifier: STORYBOARD_ID_OAUTH) as! OAuthViewController
        self.present(loginOAuthViewController, animated: false, completion: nil)
    }

}
