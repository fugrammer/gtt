//
//  MonthlySearchTableViewCell.swift
//  GTT
//
//  Created by THAM, THOMAS on 22/6/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit

class MonthlySearchTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
