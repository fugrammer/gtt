//
//  Constants.swift
//  gtt_test
//
//  Created by KHOR, LE YI on 6/12/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import Foundation
import UIKit


//MARK: - Login API
let BASE_URL_AUTH = "https://iapi-test.merck.com/authentication-service/v2/"
let API_AUTHORIZATION = "authorize"
let API_ACCESSTOKEN = "token"
let API_USERPROFILE = "userinfo"
let CLIENT_ID = "y1lENl3Jq3GlHVuVtD9ZbI20fGf75vBw"
let CLIENT_SECRET = "MVHsGHi7WlunZ1FW"
let CLIENT_BASE = "\(CLIENT_ID):\(CLIENT_SECRET)".toBase64()

//MARK: - Internal directory API
let BASE_URL_INTERNAL_DIR = "https://iapi-test.merck.com/internal-directory/v2"
let AUTH_KEY = "Basic cmFqZW5hcnU6bWVyY2tAMTI="

//MARK: Login Storyboard
let STORYBOARD_ID_OAUTH = "OAuthLogin"
let STORYBOARD_ID_LOGIN = "Login"
let URL_OAUTH = "https://www.google.com"

//MARK: - Fonts
let FONT_TITLE = UIFont(name: "Helvetica Neue Medium", size: 18)
let TASK_FONT = UIFont(name: "Helvetica Neue-Medium", size: 5)
let TASK_BOLD = UIFont(name: "Helvetica Neue Bold", size: 5)

//MARK: - User Profile
let USER_EMAIL = "email"
let USER_ISID = "isid"
let USER_GIVENNAME = "given_name"

//MARK: - Common
let USER_LOGGED_IN = "User logged in"
let DEFAULT_SCREEN = "default_screen"
let MONTHLY_SCREEN = "monthly_screen"
let DAILY_SCREEN = "daily_screen"

//MARK: - Colour
let MERCK_GREEN = 0x00867B
let TASK_TEXT_COLOUR = 0x555F73
let DAY_TEXT_UNSELECTED_COLOR=0x828282
let DAY_TEXT_SELECTED_COLOR=0xFFFFFF
let CALENDAR_BACKGROUND_COLOR = 0xEFEFEF
let MILKYWHITE_TEXT_COLOR = 0xF2F2F2
let SEARCH_BAR_COLOUR = 0xF2F2F2
let TITLE_GRAY = 0xE0E0E0
let TITLE_TEXT_GRAY = 0x4F4F4F

//MARK: - Time
let DAY_SECONDS = 24*60*60

//MARK: - Alert messages
let ALERT_TITLE = "Alert"
let ALERT_NOINTERNET = "No internet connection found!"
let ALERT_NO_OAUTH = "Unable to establish connection to Merck Authentication! Make sure you are connected to Merck network and click Okay"
let TASK_UPDATED = "Tasks updated"


