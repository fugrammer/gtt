//
//  DailyViewControllerSearchBar.swift
//  GTT
//
//  Created by Khor Le Yi on 28/6/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import Foundation
import UIKit

extension DailyViewController: UISearchBarDelegate{
    //MARK: -Search Bar Functions
    
    func setUpSearchBar(){
        // search bar
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        // search bar color
        searchBar.barTintColor = UIColor.white
        searchBar.layer.borderColor = UIColor.white.cgColor
        searchBar.layer.borderWidth = 1

        // textfield in search bar color
        let textfield_search_controller = searchBar.value(forKey: "searchField") as! UITextField?
        textfield_search_controller?.backgroundColor = UIColor(rgb: SEARCH_BAR_COLOUR)
        
        // change search icon color
        let search_icon = textfield_search_controller?.leftView as! UIImageView
        search_icon.image = search_icon.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        search_icon.tintColor = UIColor(rgb: MERCK_GREEN)

    }
    
    /// Hide keyboard when X is clicked on search bar
    @objc func hideKeyboardWithSearchBar(bar:UISearchBar) {
        bar.resignFirstResponder()
    }
    
    // filters the tasks during search
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            is_searching = false
            view.endEditing(true)
            taskListTableView.reloadData()
            perform(#selector(hideKeyboardWithSearchBar), with: searchBar, afterDelay: 0)
        } else {
            is_searching = true
            filtered_task_ids = task_ids_sorted.filter { task_id in
                let contain_id = String(describing: task_id).lowercased().contains((searchBar.text?.lowercased())!)
                let contain_name = String(describing: task_manager.getTaskName(date: display_date, taskID: task_id)).lowercased().contains((searchBar.text?.lowercased())!)
                return contain_id || contain_name
            }
            taskListTableView.reloadData()
        }
    }
    
    func endSearch() {
        is_searching = false
        let textfield_search_controller = searchBar.value(forKey: "searchField") as! UITextField?
        textfield_search_controller?.resignFirstResponder()
        textfield_search_controller?.text = nil
    }
    
    // ends search when user touches areas outside the table
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let textfield_search_controller = searchBar.value(forKey: "searchField") as! UITextField?
        if touches.first != nil {
            textfield_search_controller?.resignFirstResponder()
        }
        super.touchesBegan(touches, with: event)
        is_searching = false
    }
}
