//
//  OAuthViewController.swift
//  gtt_test
//
//  Created by THAM, THOMAS on 6/12/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit
import Onboard

class OAuthViewController: UIViewController,UIWebViewDelegate {

    //MARK: Properties
    var timer = Timer()
    
    //MARK: - Outlets
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicatorLabel: UILabel!
    @IBOutlet weak var oauthWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestAuthorizationToken()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Accessing Merck's OAuth login page
    func requestAuthorizationToken() {
        // Specify the response type which should always be "code".
        let responseType = "code"
        
        // Set the redirect URL. Adding the percent escape characthers is necessary.
        let redirectURL = "gtt://authorized".addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
    
        // Create a random string based on the time intervale (it will be in the form linkedin12345679).
        let state = "gtt\(Int(NSDate().timeIntervalSince1970))"
        
        // Set preferred scope.
        let scope = "login_method=sso"
        
        var authorizationURL = "\(BASE_URL_AUTH+API_AUTHORIZATION)?"
        authorizationURL += "response_type=\(responseType)&"
        authorizationURL += "client_id=\(CLIENT_ID)&"
        authorizationURL += "redirect_uri=\(String(describing: redirectURL!))&"
        authorizationURL += "state=\(state)&"
        authorizationURL += "\(scope)"
        
        // Create a URL request and load it in the web view.
        let request = NSURLRequest(url: NSURL(string: authorizationURL)! as URL)
        oauthWebView.loadRequest(request as URLRequest)
        
    }
    
    
    //MARK: WebView delegates
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let url = request.url!
    
        if url.host == "authorized" {
            
            let urlString: String = url.absoluteString
            
            if urlString.range(of:"code") != nil{
                //  if url.absoluteString.rangeOfString("code") != nil {
                // Extract the authorization code.
                let urlParts = urlString.components(separatedBy:"?")
                let code = urlParts[1].components(separatedBy: "=")[1].components(separatedBy: "&")[0]
                print("code: \(code)")
                requestAccessToken(authorizationCode: code)
            }
        }
        return true
    }
    
    func requestAccessToken(authorizationCode: String) {
        
        let grantType = "authorization_code"
        let redirectURI = "gtt://authorized".addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        
        var parameters = "grant_type=\(grantType)&"
        parameters += "code=\(authorizationCode)&"
        parameters += "redirect_uri=\(String(describing: redirectURI!))"
        let parameterData = parameters.data(using: String.Encoding.utf8)
        
        var header = [String: String]()
        header["Content-Type"] = "application/x-www-form-urlencoded"
        header["Authorization"] = "Basic \(CLIENT_BASE)"
        
        APIHandler().callWebService(url: BASE_URL_AUTH+API_ACCESSTOKEN, parameters: parameterData!, headers: header, method: "POST") { (responseData, error, success, alertMessage) in
            
            if success {
                let responseDict: NSDictionary?
                do {
                    responseDict = try JSONSerialization.jsonObject(with: (responseData as! NSData) as Data,
                                                                    options: []) as? NSDictionary
                    
                    guard let responseDict = responseDict else {
                        return
                    }
                    
                    let accessToken = "Bearer "+String(describing: responseDict.object(forKey: "access_token")!)
                    self.getUserProfile(accessToken: accessToken)
                    
                } catch  {
                    print("Support - error trying to convert data to JSON")
                    
                    return
                }
            } else {
                
            }
        }
    }
    
    /// Get user ISID, EMAIL and FULLNAME from Merck internal directory
    func getUserProfile(accessToken: String) {
        
        var header = [String: String]()
        header["Content-Type"] = "application/json"
        header["Authorization"] = accessToken
        
        APIHandler().callWebService(url: BASE_URL_AUTH+API_USERPROFILE, parameters: NSData() as Data, headers: header, method: "GET") { (responseData, error, success, alertMessage) in
            if success {
                let responseDict: NSDictionary?
                do {
                    responseDict = try JSONSerialization.jsonObject(with: (responseData as! NSData) as Data,
                                                                    options: []) as? NSDictionary
                    
                    guard let responseDict = responseDict else {
                        return
                    }
                    
                    // Stores user information for future access
                    let ISID = responseDict.object(forKey: "isid") ?? ""
                    let NAME = responseDict.object(forKey: "given_name") ?? ""
                    let EMAIL = responseDict.object(forKey: "email") ?? ""
                    UserDefaults.standard.set(ISID, forKey:USER_ISID)
                    UserDefaults.standard.set(EMAIL, forKey:USER_EMAIL)
                    UserDefaults.standard.set(NAME, forKey:USER_GIVENNAME)
                    UserDefaults.standard.set(true, forKey: USER_LOGGED_IN)
                    
                    // Obtain user profile picture for login page
                    APIHandler().getProfilePicture()
                    
                    // Disable reset timer for connectivity check
                    self.timer.invalidate()
                    
                    guard let default_screen = UserDefaults.standard.string(forKey: DEFAULT_SCREEN) else{
                        DispatchQueue.main.async {
                            //perform tutorial
                                self.performSegue(withIdentifier: "OAuthToParent", sender: nil)
                        }
                        return
                    }
                    if default_screen == DAILY_SCREEN{
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "OAuthToParent", sender: nil)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "OAuthToParent", sender: nil)
                        }
                    }
                } catch  {
                    print("Support - error trying to convert data to JSON")
                    
                    return
                }
            } else {
                
            }
        }
    }

   
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.errorLoading), userInfo: nil, repeats: false)
    }
    
    @objc func errorLoading(){
        Helper.AlertView.showAlertMessage(viewController: self, alertTitle: ALERT_TITLE, alertMessage: ALERT_NO_OAUTH, errorCallBack: retryConnection)
    }
    
    // Retry connection after alerting user on connectivity
    func retryConnection(_ alertAction : UIAlertAction){
        requestAuthorizationToken()
    }
    
    // Stop activityIndicator when webpage is loaded
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
        timer.invalidate()
        activityIndicatorLabel.isHidden = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
