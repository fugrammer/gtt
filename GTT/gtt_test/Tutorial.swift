//
//  Tutorial.swift
//  GTT
//
//  Created by KHOR, LE YI on 7/10/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Onboard

protocol Tutorial {
    func presentTutorial()
}

extension Tutorial where Self:UIViewController{
    /// Present onboarding screens
    func presentTutorial(){
        let bundle = Bundle.main
        let moviePath = bundle.path(forResource: "Comp 1_4", ofType: "mp4")
        let movieURL = NSURL(fileURLWithPath: moviePath!)
        let moviePath_two = bundle.path(forResource: "Comp 1_1", ofType: "mp4")
        let movieURL_two = NSURL(fileURLWithPath: moviePath_two!)
        
        let page1 = OnboardingContentViewController(title: "", body: "", videoURL: movieURL as URL, buttonText: "", action: nil)
        
        let page2 = OnboardingContentViewController(title: "", body: "", videoURL: movieURL_two as URL, buttonText: "end"){
            self.dismiss(animated: true)
        }
        
        page1.movesToNextViewController = true
        page2.movesToNextViewController = true
        let onboardingVC = OnboardingViewController(backgroundImage: nil, contents: [page1, page2])
        self.present(onboardingVC!, animated: true)
    }
    
}

extension MonthlyViewController : Tutorial{}
