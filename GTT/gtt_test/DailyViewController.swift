//
//  ViewController.swift
//  LAL
//
//  Created by Khor Le Yi on 12/6/17.
//  Copyright © 2017 Khor Le Yi. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class DailyViewController: UIViewController, NVActivityIndicatorViewable{

    
    //MARK: - Constants
    let date_formatter = DateFormatter()
    let cell_identifier = "TaskTableViewCell"
    let codeBorder = CALayer()
    let descriptionBorder = CALayer()
    let hoursBorder = CALayer()
    let width = CGFloat(2.0)
    let dateComponents = DateComponents()
    
    //MARK: - Variables
    var display_date = Date()
    var task_manager = TaskHelper()
    var task_ids = [Int]()
    var task_ids_sorted = [Int]()
    var filtered_task_ids = [Int]()
    var total_hours = 0.0
    var is_searching = false
    var loaded_db = false
    var calendar = Calendar.init(identifier: .gregorian)
    var CURRENT_MONTH : Int!
    var CURRENT_YEAR  : Int!
    var CURRENT_DAY  : Int!
    
    //MARK: - Outlets
    @IBOutlet weak var monthBarView: UIView!
    @IBOutlet weak var dateMonthYearLabel: UILabel!
    @IBOutlet weak var taskListBackgroundView: UIView!
    @IBOutlet weak var taskListTableView: UITableView!
    @IBOutlet weak var totalHoursLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var codeHeader: UILabel!
    @IBOutlet weak var descriptionHeader: UILabel!
    @IBOutlet weak var hoursHeader: UILabel!
    @IBOutlet weak var previousDayButton: UIButton!
    @IBOutlet weak var nextDayButton: UIButton!
    
    
    //MARK: - Initialisation
    override func viewDidLoad() {
        super.viewDidLoad()

        task_manager.setLocalDB()
        loadAllTasks()
        setUpTemplate()
        setUpSearchBar()
        setUpNavigationBar()
        
        // set default screen as daily view
        UserDefaults.standard.set(DAILY_SCREEN,forKey: DEFAULT_SCREEN)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // get all the id for current time period
    func loadAllTasks() {
        task_ids = task_manager.getAllTaskIds(date: display_date)
        sortTasks()
        setTotalHours()
    }
    
    /// Sort tasks based on hours
    func sortTasks(){
        // is empty when table was initialised and tasks were sorted, now it's updating table with new value
        if task_ids.isEmpty {
            task_ids = task_ids_sorted
            task_ids_sorted.removeAll()
        }
        
        while !task_ids.isEmpty {
            // get task_id of first index of task_id array then remove task
            let selected_task_id : Int
            selected_task_id = task_ids[0]
            task_ids.remove(at: 0)
            
            // get hours for task_id
            let task_hour_string : String
            task_hour_string = task_manager.getDateHours(date: display_date, taskID: selected_task_id)
            let task_hour : Double
            task_hour = Double(task_hour_string)!
            
            // if task_id_sorted is empty, append straightaway, end iteration
            if task_ids_sorted.isEmpty {
                task_ids_sorted.append(selected_task_id)
                continue
            }
            
            // if task_id_sorted is not empty, append to back of array if 0, end iteration
            if Double(task_hour) == 0 {
                task_ids_sorted.append(selected_task_id)
                continue
            }
            
            var compare_index = 0
            var compare_task_id : Int
            var compare_task_hours : Double!
            let max_index = task_ids_sorted.count - 1
            
            // compare non-zero selected task_ids
            while compare_task_hours != 0 {
                compare_task_id = task_ids_sorted[compare_index]
                compare_task_hours = Double(task_manager.getDateHours(date: display_date, taskID: compare_task_id))
                
                // replace index if larger
                if (task_hour > compare_task_hours){
                    task_ids_sorted.insert(selected_task_id, at: compare_index)
                    break
                }
                
                // append if end of sorted array
                if (compare_index == max_index) {
                    task_ids_sorted.append(selected_task_id)
                    break
                }
                
                compare_index += 1
            }
        }
        taskListTableView.reloadData()
    }
    
    
    //MARK: -Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
            
        // configure addHoursDailyView before it it presented
        case "ShowDetail":
            guard let taskDetailViewController = segue.destination as? DailyAddHoursViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedTaskCell = sender as? DisplayTaskTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = taskListTableView.indexPath(for: selectedTaskCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selected_task_id : Int
            if is_searching && searchBar.text != ""{
                selected_task_id = filtered_task_ids[indexPath.row]
            } else {
                selected_task_id = task_ids_sorted[indexPath.row]
            }
        
            // disable search bar to load AddHoursViewController
            is_searching = false

            // create task object
            let task_name = task_manager.getTaskName(date: display_date, taskID: selected_task_id)
            let task_hour = task_manager.getDateHours(date: display_date, taskID: selected_task_id)
            let task = Task(code: selected_task_id, description: task_name, hours: Double(task_hour)!)
            
            // set editing task in addHoursViewController to selected task
            taskDetailViewController.task = task
            taskDetailViewController.total_day_hours = total_hours
            taskDetailViewController.display_date = display_date
            
        case "SwitchToMonthlySegue":
            _ = 1
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
    
    // called after saving hours
    func updateHours(sender: UIStoryboardSegue) {
        print ("update")
        guard let sourceViewController = sender.source as? DailyAddHoursViewController else {
            fatalError("Sender not DailyAddHoursViewController")
        }
        let passed_in_task = sourceViewController.task
        task_manager.setHours(date: display_date, taskID: (passed_in_task?.code)!, hours: (passed_in_task?.hours)!)

        endSearch()
        sortTasks()
        setTotalHours()
        taskListTableView.reloadData()
    }
    
    //MARK: -UI functions
    func setUpTemplate(){
        
        // setting up look of calendar
        taskListBackgroundView.backgroundColor = .white
        taskListBackgroundView.layer.cornerRadius = 0.0
        var maskPath = UIBezierPath(roundedRect: taskListBackgroundView.bounds, byRoundingCorners: [.bottomLeft,.bottomRight], cornerRadii: CGSize(width: 10.0, height: 0.0))
        var maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        taskListBackgroundView.layer.mask = maskLayer
        
        monthBarView.layer.cornerRadius = 0.0
        maskPath = UIBezierPath(roundedRect: monthBarView.bounds, byRoundingCorners: [.topLeft,.topRight], cornerRadii: CGSize(width: 10.0, height: 0.0))
        maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        monthBarView.layer.mask = maskLayer
        
        // date
        date_formatter.dateStyle = .medium
        date_formatter.timeStyle = .none
        date_formatter.locale = Locale(identifier: "en_US")
        dateMonthYearLabel.text = date_formatter.string(from: display_date)

        // add bottom line to table header
        codeBorder.borderColor = UIColor.darkGray.cgColor
        codeBorder.borderWidth = width
        codeBorder.frame = CGRect(x: 0, y: codeHeader.frame.size.height - width, width:  codeHeader.frame.size.width, height: codeHeader.frame.size.height)
        codeHeader.layer.addSublayer(codeBorder)
        codeHeader.layer.masksToBounds = true
        
        descriptionBorder.borderColor = UIColor.darkGray.cgColor
        descriptionBorder.borderWidth = width
        descriptionBorder.frame = CGRect(x: 0, y: descriptionHeader.frame.size.height - width, width:  descriptionHeader.frame.size.width, height: descriptionHeader.frame.size.height)
        descriptionHeader.layer.addSublayer(descriptionBorder)
        descriptionHeader.layer.masksToBounds = true
        
        hoursBorder.borderColor = UIColor.darkGray.cgColor
        hoursBorder.borderWidth = width
        hoursBorder.frame = CGRect(x: 0, y: hoursHeader.frame.size.height - width, width:  hoursHeader.frame.size.width, height: hoursHeader.frame.size.height)
        hoursHeader.layer.addSublayer(hoursBorder)
        hoursHeader.layer.masksToBounds = true
    }
    
    // set transparency of AddHoursViewController to overlap over view controller
    func showModal() {
        let modalViewController = DailyAddHoursViewController()
        modalViewController.modalPresentationStyle = .overCurrentContext
        present(modalViewController, animated: true, completion: nil)
    }
    
    //MARK: -Support functions
    func setTotalHours() {
        total_hours = 0.0
        for each_task_id in task_ids_sorted{
            let hour = task_manager.getDateHours(date: display_date, taskID: each_task_id)
            total_hours += Double(hour)!
        }
        self.totalHoursLabel.text = String(format: "%.1f", total_hours) + " hrs"
    }

}
