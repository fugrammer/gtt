////
////  TaskTableViewController.swift
////  LAL
////
////  Created by Khor Le Yi on 14/6/17.
////  Copyright © 2017 Khor Le Yi. All rights reserved.
////
//
//import UIKit
//
//class DailyTaskTableViewController: UITableViewController {
//    
//    //MARK: - To change
//    var number_of_task = 10
//    var code_label = "MX1005"
//    var description_label = "Hours"
//    var hours_label = "1.0"
//    
//    //MARK: - Variables
//    let cellIdentifier = "MealTableViewCell"
//    
//    //MARK: - Initialisation
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Uncomment the following line to preserve selection between presentations
//        // self.clearsSelectionOnViewWillAppear = false
//
//        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    // MARK: - Table view data source
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//    
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return number_of_task
//    }
//    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        // Table view cells are reused and should be dequeued using a cell identifier.
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DisplayTaskTableViewCell  else {
//            fatalError("The dequeued cell is not an instance of TaskTableViewCell.")
//        }
//        
//        // Fetches the appropriate meal for the data source layout.
//        //let meal = meals[indexPath.row]
//        
//        cell.codeLabel.text = code_label
//        cell.descriptionLabel.text = description_label
//        cell.hoursLabel.text = hours_label
//        
//        return cell
//    }
//}
