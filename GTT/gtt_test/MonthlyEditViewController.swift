//
//  ViewController.swift
//  gtt_test
//
//  Created by THAM, THOMAS on 6/9/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit
import JTAppleCalendar
import NVActivityIndicatorView

class MonthlyEditViewController: UIViewController, UIGestureRecognizerDelegate,NVActivityIndicatorViewable {
    
    //MARK: - Properties
    var days = [MonthlyCalendarDay]()
    let reuseIdentifier = "CalendarCell"
    var TOTAL_DAYS : Int!
    var numberDaysSelected = 0
    var currentSelected = SelectState.SelectAll
    var taskManager = TaskHelper()
    var alerting = false
    var outcome = false
    var calendar = Calendar.init(identifier: .gregorian)
    var timer = Timer()
    var delay_timer = Timer()
    var input_value = 0.0
    
    //MARK: - Passed in properties
    var CURRENT_MONTH : Int!
    var CURRENT_YEAR  : Int!
    var TASK : Task!
    
    //MARK: - Constants
    enum SelectState{
        case SelectAll
        case DeselectAll
    }
    let MAXIMUM_HOUR_VALUE = 24.0
    let MINIMUM_HOUR_VALUE = 0.0
    let increment_value = 0.1
    let increment_more_value = 1.0
    
    //MARK: - Outlets
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var totalHoursLabel: UILabel!
    @IBOutlet weak var monthYearLabel: UILabel!
    @IBOutlet weak var calendarCollectionView: UICollectionView!
    @IBOutlet weak var monthBarView: UIView!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var editHoursLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var totalLbel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var dayLabelView: UIView!
    @IBOutlet weak var calendarLoadingView: UIView!
    @IBOutlet weak var calendarLoadingIndicator: NVActivityIndicatorView!
    @IBOutlet weak var calendarLoadingText: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    
    //MARK: - Actions
    
    /// Check if hours entered by user are valid, then update them
    @IBAction func savingTask(_ sender: UIButton) {
        outcome = false
        let size = CGSize(width: 100, height: 100)
        startAnimating(size , message: "Saving task", type: NVActivityIndicatorType.orbit, minimumDisplayTime: 1,textColor: UIColor.white)
        DispatchQueue.background(delay: 0.0, background: {
            self.outcome = self.validateHours()
            }, completion: {
                if self.outcome == true{
                    self.performSegue(withIdentifier: "MonthlyEditSave", sender: nil)
                }else{
                    Drop.down("Unable to save. Hours worked for some days exceeded 24 hours")
                    self.switchToSelectAll()
                    self.calendarCollectionView.selectItem(at: nil, animated: false, scrollPosition: [])
                    self.calendarCollectionView.reloadData()
                    self.stopAnimating()
                }
        })
    }
    
    // User clicked on select all/deselect all button
    @IBAction func selectAllBUtton(_ sender: UIButton) {
        switch (currentSelected){
        case .SelectAll:
            for day in days{
                if day.day != 1 && day.day != 7 && day.enabled == true{
                    day.selected = true
                }
            }
            switchToDeselectAll()
        case .DeselectAll:
            switchToSelectAll()
        }
        calendarCollectionView.reloadData()
    }
    
    /// Back to search view
    @IBAction func backButton(_ sender: UIButton) {
        self.dismiss(animated:true, completion: nil)
    }
    
    @IBAction func saveUpdates(_ sender: UIButton) {
        
    }
    
    //MARK: - Initialisation
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        calendar.timeZone = TimeZone(abbreviation: "GMT")!
        calendarCollectionView.allowsMultipleSelection = true
        
        // Set up colors
        saveButton.backgroundColor = UIColor(rgb:CALENDAR_BACKGROUND_COLOR)
        monthBarView.backgroundColor = UIColor(rgb:CALENDAR_BACKGROUND_COLOR)
        monthYearLabel.textColor = UIColor(rgb: TITLE_TEXT_GRAY)
        calendarCollectionView.backgroundColor = UIColor(rgb:CALENDAR_BACKGROUND_COLOR)
        dayLabelView.backgroundColor = UIColor(rgb:CALENDAR_BACKGROUND_COLOR)
        totalLbel.textColor = UIColor(rgb:MILKYWHITE_TEXT_COLOR)
        totalHoursLabel.textColor = UIColor(rgb: MILKYWHITE_TEXT_COLOR)
        selectButton.setTitleColor(UIColor(rgb:MILKYWHITE_TEXT_COLOR), for: [.normal])
        backButton.setTitleColor(UIColor(rgb:MILKYWHITE_TEXT_COLOR), for: [.normal])
        
        // initialise minus button
        let long_gesture_minus = UILongPressGestureRecognizer(target: self, action: #selector(MonthlyEditViewController.longTapMinus(_:)))
        minusButton.addGestureRecognizer(long_gesture_minus)
        minusButton.addTarget(self, action: #selector(self.normalTap), for: .touchUpInside)
        
        // initialise add button
        let long_gesture_add = UILongPressGestureRecognizer(target: self, action: #selector(MonthlyEditViewController.longTapAdd(_:)))
        addButton.addGestureRecognizer(long_gesture_add)
        addButton.addTarget(self, action: #selector(self.normalTap), for: .touchUpInside)

        
        //Make sure screen was prepared correctly
        guard CURRENT_YEAR != nil else{
            fatalError()
        }
        
        guard CURRENT_MONTH != nil else {
            fatalError()
        }
        
        guard (CURRENT_MONTH >= 1) && (CURRENT_MONTH <= 12) && (CURRENT_YEAR >= 1992) && (CURRENT_YEAR <= 9999) else{
            fatalError("Invalid month/year for editing")
        }
        
        taskManager.setLocalDB()
        
        setUpCalendar()
        
        loadCalendar()
        
        updateTotalHours()
    }
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    override func didReceiveMemoryWarning() {

        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private functions
    
    // called upon one tap
    func normalTap(_ sender: UIGestureRecognizer){
        
        // checks if add or subtract button
        switch (sender){
        case minusButton:
            minusValue()
            break
        case addButton:
            addValue()
            break
        default:
            break
        }
    }
    
    // called upon long tap of minus button
    func longTapMinus(_ sender: UIGestureRecognizer){
        if sender.state == .ended {
            timer.invalidate()
            delay_timer.invalidate()
        }
        else if sender.state == .began {
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(MonthlyEditViewController.minusValue), userInfo: nil, repeats: true)
            delay_timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false){ (_) in
                self.timer.invalidate()
                self.timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(MonthlyEditViewController.minusMoreValue), userInfo: nil, repeats: true)
            }

        }
    }
    
    // called upon long tap of add button
    func longTapAdd(_ sender: UIGestureRecognizer){
        if sender.state == .ended {
            timer.invalidate()
            delay_timer.invalidate()
        }
        else if sender.state == .began {
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(MonthlyEditViewController.addValue), userInfo: nil, repeats: true)
            delay_timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false){ (_) in
                self.timer.invalidate()
                self.timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(MonthlyEditViewController.addMoreValue), userInfo: nil, repeats: true)
            }
        }
    }
    
    // add 0.1 to input value
    func addValue() {
        if !(editHoursLabel.text!.isEmpty) {
            let currentValue = Double(editHoursLabel.text!)!;
            var new_value = currentValue + 0.1
            
            // check that value is not above maximum_input_value
            if (new_value > MAXIMUM_HOUR_VALUE) {
                new_value = MAXIMUM_HOUR_VALUE
            }
            self.editHoursLabel.text = String(new_value);
            updateHours(hours: new_value)
        }
    }
    
    // subtract 0.1 to input value
    func minusValue() {
        if !(editHoursLabel.text!.isEmpty) {
            let currentValue = Double(editHoursLabel.text!)!;
            var new_value = currentValue - 0.1
            if (new_value < MINIMUM_HOUR_VALUE) {      // check that value is not below minimum_input_value
                new_value = MINIMUM_HOUR_VALUE
            }

            self.editHoursLabel.text = String(new_value);
            updateHours(hours: new_value)
        }
    }
    
    // add 0.5 to input value
    func addMoreValue() {
        if (editHoursLabel.text?.isEmpty)!{
            input_value = 0.0
        }
        else {
            input_value = Double(editHoursLabel.text!)!;
        }
        var new_value = input_value + increment_more_value
        if (new_value > MAXIMUM_HOUR_VALUE) {      // check that value is not above maximum_input_value
            new_value = MAXIMUM_HOUR_VALUE
        }
        self.editHoursLabel.text = String(new_value);
        updateHours(hours: new_value)
    }
    
    // subtract 0.5 to input value
    func minusMoreValue() {
        if (editHoursLabel.text?.isEmpty)!{
            input_value = 0.0
        }
        else {
            input_value = Double(editHoursLabel.text!)!;
        }
        var new_value = input_value - increment_more_value
        if (new_value < MINIMUM_HOUR_VALUE) {      // check that value is not below minimum_input_value
            new_value = MINIMUM_HOUR_VALUE
        }
        
        self.editHoursLabel.text = String(new_value);
        updateHours(hours: new_value)
    }
    
    // User clicked on deselect all
    func switchToSelectAll(){
        UIView.performWithoutAnimation {
            selectButton.setTitle("Select all", for: [.normal])
        }
        for day in days{
            day.selected = false
        }
        currentSelected = .SelectAll
        numberDaysSelected = 0
        editHoursLabel.text = "0.0"
    }
    
    // User clicked on select all
    func switchToDeselectAll(){
        UIView.performWithoutAnimation {
            //selectButton.reversesTitleShadowWhenHighlighted = false
            selectButton.setTitle("Deselect all", for: [.normal])
        }
        currentSelected = .DeselectAll
        var count = 0
        var minimum = 24.0
        for day in days{
            if day.selected == true{
                count += 1
            minimum = min(minimum,day.hours)
            }
        }
        editHoursLabel.text = String(minimum)
        numberDaysSelected = count
    }
    
    /// Update total hour label
    private func updateTotalHours(){
        var hours = 0.0
        for day in days{
            hours += day.hours
        }
        totalHoursLabel.text = String(describing: hours) + " hrs"
    }
    
    /// Initialise calendar
    private func loadCalendar(){
        
        
        var dateComponents = DateComponents(year: CURRENT_YEAR, month: CURRENT_MONTH, day: 2)

        var task_hours = taskManager.getMonthHours(date: calendar.date(from: dateComponents)!, taskID: TASK.code)
        // During testing only
        for _ in task_hours.count..<TOTAL_DAYS{
            task_hours.append("0")
        }
        
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let myCalendar = Calendar(identifier: .gregorian)
        print (TOTAL_DAYS)  
        for i in 1...TOTAL_DAYS{
            guard let todayDate = formatter.date(from: "\(CURRENT_YEAR!)-\(CURRENT_MONTH!)-\(i)") else {
                let day = MonthlyCalendarDay(date: i, hours: Double(task_hours[i-1])!, day: 0, enabled: false)
                days += [day]
                print ("Error error")
                continue
            }
            let weekday = myCalendar.component(.weekday, from: todayDate)
            dateComponents.day = i
            var day : MonthlyCalendarDay
            let date = calendar.date(from:dateComponents)!
            
            day = MonthlyCalendarDay(date: i, hours: Double(task_hours[i-1])!, day: weekday,enabled: taskManager.isDayOpen(date:date),date_object:date)

            days += [day]
        }
        
        // Description of task
        descriptionLabel.text = TASK.description
    }
    
    /// Initialise appearence of calendar
    private func setUpCalendar(){
        
        // MK Code of task
        
        codeLabel.text = String(describing:TASK.code)
        
        let myCalendar = Calendar(identifier: .gregorian)

        // Get name of the month
        var month = DateComponents()
        month.month = CURRENT_MONTH
        
        let monthCalendar = myCalendar.date(from: month)
        let monthFormatter = DateFormatter()
        monthFormatter.dateFormat = "MMMM"
        
        let monthString = monthFormatter.string(from: monthCalendar!)
        
        // Get number of days in month
        
        let dateComponents = DateComponents(year: CURRENT_YEAR, month: CURRENT_MONTH)
        
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        TOTAL_DAYS = numDays
        // Update calendar title
        monthYearLabel.text = monthString+" "+String(describing: CURRENT_YEAR!)
        
        //Setting up look of calendar
        calendarCollectionView.layer.cornerRadius = 0.0
        var maskPath = UIBezierPath(roundedRect: calendarCollectionView.bounds, byRoundingCorners: [.bottomLeft,.bottomRight], cornerRadii: CGSize(width: 10.0, height: 0.0))
        var maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        calendarCollectionView.layer.mask = maskLayer
        
        monthBarView.layer.cornerRadius = 0.0
        maskPath = UIBezierPath(roundedRect: monthBarView.bounds, byRoundingCorners: [.topLeft,.topRight], cornerRadii: CGSize(width: 10.0, height: 0.0))
        maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        monthBarView.layer.mask = maskLayer
        monthBarView.backgroundColor = UIColor(rgb: TITLE_GRAY)
    }
    
    /// Update hours of each selected cell
    private func updateHours(hours : Double){
        for day in days{
            if day.selected == true{
                day.updateHours(hours: hours)
                calendarCollectionView.reloadData()
            }
        }
        updateTotalHours()
    }
    
    //MARK: - Navigation
    
    /// called after saving hours
    @IBAction func updateHours(sender: UIStoryboardSegue) {
        guard let sourceViewController = sender.source as? DailyAddHoursViewController else {
            fatalError("Sender not MonthlyEditViewController")
        }
        
        let passed_in_task = sourceViewController.task!
        
        updateHours(hours: passed_in_task.hours)
        editHoursLabel.text = String(format: "%0.1f",passed_in_task.hours)
    }
    
        
    /// Ensure that no day has more than 24 hours logged in total for all the tasks
    private func validateHours()->Bool{
        var passed = true
        var hours = Array(repeating: 0.0, count: 31)
        let dateComponents = DateComponents(year: CURRENT_YEAR, month: CURRENT_MONTH,day:2)
        let current_date = calendar.date(from: dateComponents)!
        
        let tasksArray = self.taskManager.getAllTaskIds(date:current_date)
        
        for taskID in tasksArray{
            if taskID == TASK.code{
                continue
            }
            
            for (index,hour) in taskManager.getMonthHours(date: current_date, taskID: taskID).enumerated(){
                hours[index] += Double(hour)!
            }
        }
        
        for (index,hour) in hours.enumerated(){
            if index > TOTAL_DAYS-1{
                continue
            }
            if hour + days[index].hours > MAXIMUM_HOUR_VALUE {
                passed = false
            }
        }
        return passed
    }
    
    // Revert cell to unselected state
    func deselectCell(_ cell : JTAppleCell,_ cellState: CellState){
        guard let cell = cell as? MonthlyCalendarCollectionViewCell else {
            return
        }
        cell.dateLabel.textColor = UIColor(rgb: DAY_TEXT_UNSELECTED_COLOR)
        if cell.enabled == false {
            cell.backgroundImageView.image = #imageLiteral(resourceName: "disabled_day")
            return
        }
        switch (cellState.day.rawValue){
        case 1, 7:
            cell.backgroundImageView.image = #imageLiteral(resourceName: "weekend")
        default:
            cell.backgroundImageView.image = #imageLiteral(resourceName: "weekday")
        }
    }
    
    // Change cell to selected state
    func selectCell(_ cell : JTAppleCell, _ cellState:CellState){
        guard let cell = cell as? MonthlyCalendarCollectionViewCell else {
            return
        }
        
        cell.dateLabel.textColor = UIColor(rgb: DAY_TEXT_SELECTED_COLOR)
        cell.backgroundImageView.image = #imageLiteral(resourceName: "selected_day")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch (segue.identifier ?? ""){
        case "MonthlyEditSave":
            // User saving updates
            print (days.count)
            
            // Uncomment the following code when array update has been implemented
            
//            taskManager.setHoursArray(days:days, taskID: <#Int#>)
            
            // Delete the following code when array update has been implemented
            for day in days{
                if day.enabled == true{
                    taskManager.setHours(date: day.date_object, taskID: TASK.code, hours: day.hours)
                }
            }
            let viewController = segue.destination as! ParentViewController;
            //message = "Tasks updated"
            viewController.message = TASK_UPDATED
            viewController.CURRENT_MONTH = CURRENT_MONTH
            viewController.CURRENT_YEAR = CURRENT_YEAR
            self.stopAnimating()
        default:
            // Might be obsolete
            let viewController = segue.destination as! DailyAddHoursViewController
            let task = Task(code: TASK.code, description: TASK.description, hours: 0)
            viewController.total_day_hours = 0.0
            viewController.task = task
        }
        
    }

}

