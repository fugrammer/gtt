//
//  Helper.swift
//  GTT
//
//  Created by Thomas Tham on 30/6/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import Foundation
import UIKit

enum LoginValidatorError: Error {
    case EmptyString
    case EmptyPassword
    case InvalidEmailId
}

class Helper {
    
    //Internet Connection Check
    struct InternetConnection {
        
        static func isConnectedToInternet() -> Bool {
            let reachability = Reachability()!
            
            if(reachability.isReachableViaWiFi) {
                
                return true
            } else if (reachability.isReachableViaWWAN) {
                
                return true
            } else {
                
                return false
            }
        }
    }
    
    // Global Alertview
    struct AlertView {
        static func showAlertMessage(viewController: UIViewController, alertTitle:String, alertMessage:String) -> Void {
            let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            viewController.present(alert, animated: true, completion: nil)
        }
        static func showAlertMessage(viewController: UIViewController, alertTitle:String, alertMessage:String, errorCallBack : @escaping (UIAlertAction)->Void) -> Void {
            let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: errorCallBack))
            viewController.present(alert, animated: true, completion: nil)
        }
    }
}
