//
//  ViewController.swift
//  gtt_test
//
//  Created by THAM, THOMAS on 6/9/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit
import JTAppleCalendar
import NVActivityIndicatorView
import AwesomeSpotlightView
import Onboard
class MonthlyViewController: UIViewController, NVActivityIndicatorViewable{
    
    //MARK: - Properties
    var sectionNumber = 5
    var days = [MonthlyCalendarDay]()
    var tasks = [Task]()
    var day_selected : Date? = nil
    var numberDaysSelected = 0
    var currentSelected = SelectState.SelectAll
    var taskManager = TaskHelper()
    var message : String?
    var month_total_hours = 0.0
    var months_total_hours = [Int:Double]()
    static var lastSection : Int?
    let cellIdentifier = "taskTableViewCell"
    let reuseIdentifier = "CalendarCellView"
    var loaded_db = false
    let activityData = ActivityData()
    var calendar = Calendar.init(identifier: .gregorian)
    static var all_days = [Date:MonthlyCalendarDay]() // For caching days for monthlyview
    
    //MARK: - Passed in properties
    
    var CURRENT_MONTH : Int!
    var CURRENT_YEAR  : Int!
    var START_DATE : Date?
    var END_DATE : Date?
    var TASK_CODE: Int!
    var TASK_DESCRIPTION: String!
    
    //MARK: - Constants
    enum SelectState{
        case SelectAll
        case DeselectAll
    }
    let MAXIMUM_HOUR_VALUE = 24.0
    let MINIMUM_HOUR_VALUE = 0.0
    let OFFSET = 0
    
    //MARK: - Outlets
    @IBOutlet weak var monthYearLabel: UILabel!
    @IBOutlet weak var calendarCollectionView: JTAppleCalendarView!
    @IBOutlet weak var monthBarView: UIView!
    @IBOutlet weak var taskTableView: UITableView!
//    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var chooseTaskButton: UIButton!
    @IBOutlet weak var monthBarStackView: UIStackView!
    @IBOutlet weak var dayLabelView: UIView!
    @IBOutlet weak var leftButton : UIButton!
    @IBOutlet weak var rightButton : UIButton!
    @IBOutlet weak var totalHoursLabel: UILabel!
    @IBOutlet weak var totalHoursTextLabel: UILabel!
    @IBOutlet weak var calendarLoadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var calendarLoadingText: UILabel!
    @IBOutlet weak var calendarLoadingView: UIView!
    @IBOutlet weak var toggleButton: UIButton!
    
    //MARK: - Actions
    
    /// Switch to previous month
    @IBAction func previousMonth(_ sender: UIButton?) {
        if CURRENT_MONTH == 1{
            CURRENT_MONTH = 12
            CURRENT_YEAR! -= 1
        } else {
            CURRENT_MONTH! -= 1
        }
    
        if (calendarCollectionView.currentSection()==0 || calendarCollectionView.currentSection() == 1){
            leftButton.isEnabled = false
        }
        calendarCollectionView.scrollToSegment(SegmentDestination.previous, triggerScrollToDateDelegate: true, animateScroll: true, extraAddedOffset: CGFloat((calendarCollectionView.currentSection()!-1)*(OFFSET)), completionHandler: nil)
    }
    
    /// Switch to next month
    @IBAction func nextMonth(_ sender: UIButton?) {
        if CURRENT_MONTH == 12{
            CURRENT_MONTH = 1
            CURRENT_YEAR! += 1
        } else {
            CURRENT_MONTH! += 1
        }
        if (calendarCollectionView.currentSection()==0 || calendarCollectionView.currentSection() == 1){
            leftButton.isEnabled = false
        }
       
        calendarCollectionView.scrollToSegment(SegmentDestination.next, triggerScrollToDateDelegate: true, animateScroll: true, extraAddedOffset: CGFloat((calendarCollectionView.currentSection()!+1)*(OFFSET)), completionHandler: nil)
    }

    
    //MARK: - Initialisation
    override func viewDidLoad() {
        super.viewDidLoad()
        

        calendar.timeZone = TimeZone(abbreviation: "GMT")!
        
        initialiseUI()
        
        // Get current month and year
        let date = Date()
        
        //Make sure screen was prepared correctly
        if CURRENT_YEAR == nil{
             CURRENT_YEAR = calendar.component(.year, from: date)
        }
        
        if CURRENT_MONTH == nil{
            CURRENT_MONTH = calendar.component(.month, from: date)
        }
        
        taskManager.setLocalDB()
        let start_end_dates = taskManager.getOpenDates()
        START_DATE = start_end_dates["startDate"]
        END_DATE = start_end_dates["endDate"]
        
        DispatchQueue.background(delay: 0, background: {
            self.loadData()
        }, completion:{
            self.scrollToCurrentMonth()
            self.loadTaskTable(nil)
        })
        UserDefaults.standard.set(MONTHLY_SCREEN,forKey: DEFAULT_SCREEN)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Start loading animation if page is loading for the first time
        if loaded_db == false{
            loaded_db = true
            startAnimating(CGSize(width:50,height:50), message: "Loading", type: NVActivityIndicatorType.orbit, minimumDisplayTime:0, backgroundColor: UIColor(rgb:MERCK_GREEN), textColor: UIColor.white)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        taskTableView.layer.cornerRadius = 0.0
        let maskPath2 = UIBezierPath(roundedRect: taskTableView.bounds, byRoundingCorners: [.bottomLeft,.bottomRight], cornerRadii: CGSize(width: 10.0, height: 0.0))
        let maskLayer2 = CAShapeLayer()
        maskLayer2.path = maskPath2.cgPath
        taskTableView.layer.mask = maskLayer2
    }
    
    /// Scroll to current month when view is presented
    @objc private func scrollToCurrentMonth(){
        var dateComponents = DateComponents()
        dateComponents.month = CURRENT_MONTH
        dateComponents.year = CURRENT_YEAR
        dateComponents.day = 3
        calendarCollectionView.scrollToDate(calendar.date(from: dateComponents)!, triggerScrollToDateDelegate: true, animateScroll: false, preferredScrollPosition: UICollectionViewScrollPosition.centeredHorizontally, extraAddedOffset: 0.0){
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        
    //MARK: - Private functions
    
    /// Load tasks and hours
    func loadData(){
        //Loading all the data
        self.months_total_hours = [:]
        self.month_total_hours = 0
        
        var startdate = self.START_DATE!
        let end_date = self.END_DATE!
        while startdate <= end_date{
            var totalHours = 0.0
            for taskID in self.taskManager.getAllTaskIds(date: startdate){
                totalHours += Double(self.taskManager.getDateHours(date: startdate, taskID: taskID))!
            }
            let date = calendar.component(.day, from: startdate)
            let day = calendar.component(.weekday, from: startdate)
            MonthlyViewController.all_days[startdate] = MonthlyCalendarDay(date: date, hours: totalHours, day: day, enabled: self.taskManager.isDayOpen(date: startdate), date_object: startdate)
            let current_month = calendar.component(.month, from: startdate)
            if  current_month == CURRENT_MONTH{
                self.month_total_hours += totalHours
            }
            if let _ = months_total_hours[current_month]{
                months_total_hours[current_month]! += totalHours
            } else {
                months_total_hours[current_month] = totalHours
            }
            startdate = calendar.date(byAdding: .day, value: 1, to: startdate)!
        }
    }

    // Initialise appearence of calendar
    func setUpCalendar(){

        let myCalendar = Calendar(identifier: .gregorian)
        var month = DateComponents()
        month.month = CURRENT_MONTH
        
        let monthCalendar = myCalendar.date(from: month)
        let monthFormatter = DateFormatter()
        monthFormatter.dateFormat = "MMMM"
        
        let monthString = monthFormatter.string(from: monthCalendar!)
        
        // Update calendar title
        monthYearLabel.text = monthString+" "+String(describing: CURRENT_YEAR!)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch (segue.identifier ?? ""){
        case "EditTaskSegue":
            // Modify task
            guard let viewController = segue.destination as? MonthlyEditViewController else {
                fatalError("Unexpected destination \(segue.destination)")
            }
            guard let selectedTaskCell = sender as? DisplayTaskTableViewCell else{
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = taskTableView.indexPath(for: selectedTaskCell) else{
                fatalError("Cell selected not part of table")
            }
            viewController.CURRENT_MONTH = CURRENT_MONTH
            viewController.CURRENT_YEAR = CURRENT_YEAR
            viewController.TASK = tasks[indexPath.row]
        case "SwitchToDailySegue":
            _ = 1
        default:
            // Task selection
            guard let viewController = segue.destination as? MonthlySearchViewController else {
                fatalError("Unexpected destination \(segue.destination)")
            }
            MonthlyViewController.lastSection = calendarCollectionView.currentSection()
            viewController.CURRENT_MONTH = CURRENT_MONTH
            viewController.CURRENT_YEAR = CURRENT_YEAR
        }
    }
    
    private func initialiseUI(){
        
        
        calendarCollectionView.scrollingMode = .stopAtEachCalendarFrameWidth
        calendarCollectionView.allowsMultipleSelection = false
        
        dayLabelView.backgroundColor = UIColor(rgb: 0xFFFFFF)
        monthBarView.backgroundColor = UIColor(rgb:MERCK_GREEN)
        chooseTaskButton.backgroundColor = UIColor(rgb:MERCK_GREEN)
        
        // Setting up look of calendar
//        var maskPath = UIBezierPath(roundedRect: calendarCollectionView.bounds, byRoundingCorners: [.bottomLeft,.bottomRight], cornerRadii: CGSize(width: 10.0, height: 0.0))
//        
//        var maskLayer = CAShapeLayer()
        
        monthBarView.layer.cornerRadius = 0.0
        let maskPath = UIBezierPath(roundedRect: monthBarView.bounds, byRoundingCorners: [.topLeft,.topRight], cornerRadii: CGSize(width: 10.0, height: 0.0))
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        monthBarView.layer.mask = maskLayer
        
        
    }
    
}

