//
//  DateHelper.swift
//  GTT
//
//  Created by THAM, THOMAS on 6/30/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit

class DateHelper{
    
    static var calendar = Calendar.init(identifier: .gregorian)
    
    /// Help converting year/month/day to GMT format date object
    static func dateFromComponents(year: Int, month: Int, day: Int) -> Date{
        calendar.timeZone = TimeZone(abbreviation: "GMT")!
        let dateComponents = DateComponents(year: year, month: month,day:day)
        let date = calendar.date(from: dateComponents)!
        return date
    }
}
