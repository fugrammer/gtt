//
//  MonthlySearchViewController.swift
//  GTT
//
//  Created by Khor Le Yi on 22/6/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import UIKit
import AwesomeSpotlightView

/// Search view for choosing task.
class MonthlySearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate{

    //MARK: - Variables
    let cell_identifier = "SearchTableViewCell"
    let codeBorder = CALayer()
    let descriptionBorder = CALayer()
    let hoursBorder = CALayer()
    let width = CGFloat(2.0)
    
    var search_tasks = [SearchTask]()
    var filtered_search_tasks = [SearchTask]()
    var task_manager = TaskHelper()
    var hours_for_current_date = [String:String]()
    var is_searching = false
    var all_taskID = [Int]()
    
    // Passed in properties
    var CURRENT_MONTH : Int!
    var CURRENT_YEAR : Int!

    @IBOutlet weak var searchTaskListTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var codeHeader: UILabel!
    @IBOutlet weak var descriptionHeader: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        task_manager.setLocalDB()
        loadTaskDB()
        setUpTemplate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Table View Functions
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if is_searching {
            return filtered_search_tasks.count
        }
        var dateComponents = DateComponents()
        dateComponents.month = CURRENT_MONTH
        dateComponents.year = CURRENT_YEAR
        dateComponents.day = 2
        let date = Calendar.current.date(from: dateComponents)
        return task_manager.getNumberOfTasks(date:date!)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cell_identifier, for: indexPath) as? MonthlySearchTableViewCell  else {
            fatalError("The dequeued cell is not an instance of TaskTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let task : SearchTask
        if is_searching {
            task = filtered_search_tasks[indexPath.row]
        } else {
            task = search_tasks[indexPath.row]
        }
        cell.codeLabel.text = String(describing: task.code)
        cell.descriptionLabel.text = task.description
        return cell
    }
    
    /// Load tasks from DB
    private func loadTaskDB() {
        let current_date = DateHelper.dateFromComponents(year: CURRENT_YEAR, month: CURRENT_MONTH, day: 2)
        all_taskID = task_manager.getAllTaskIds(date: current_date)
        var task_description = ""
        for each_taskID in all_taskID {
            task_description = task_manager.getTaskName(date: current_date, taskID: each_taskID)
            guard let search_task = SearchTask(code : each_taskID, description : task_description) else {
                fatalError("Unable to instantiate task : \(each_taskID)")
            }
            search_tasks += [search_task]
        }
    }
    
    //MARK: -Navigation
    
    // This method lets you configure a view controller before it's presented.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
            
        switch (segue.identifier ?? ""){
            
        case "EditHours":
            
            guard let viewController = segue.destination as? MonthlyEditViewController else {
                fatalError("Unexpected destination \(segue.destination)")
            }
            
            guard let selectedTaskCell = sender as? MonthlySearchTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = searchTaskListTableView.indexPath(for: selectedTaskCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selected_task : SearchTask
            
            if is_searching && searchBar.text != ""{
                selected_task = filtered_search_tasks[indexPath.row]
            } else {
                selected_task = search_tasks[indexPath.row]
            }
            viewController.CURRENT_MONTH = CURRENT_MONTH!
            viewController.CURRENT_YEAR = CURRENT_YEAR!
            let TASK = Task(code: selected_task.code, description: selected_task.description, hours: 0)
            viewController.TASK = TASK
                    
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: -Search Bar Functions
    

    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            is_searching = false
            view.endEditing(true)
            searchTaskListTableView.reloadData()
        } else {
            is_searching = true
            filtered_search_tasks = search_tasks.filter { task_id in
                let contain_id = String(describing: task_id.code).lowercased().contains((searchBar.text?.lowercased())!)
                let contain_name = String(describing: task_id.description).lowercased().contains((searchBar.text?.lowercased())!)
                return contain_id || contain_name
            }
            searchTaskListTableView.reloadData()
        }
    }
    

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let textfield_search_controller = searchBar.value(forKey: "searchField") as! UITextField?
        if touches.first != nil {
            textfield_search_controller?.resignFirstResponder()
        }
        super.touchesBegan(touches, with: event)
        is_searching = false
    }

    //MARK: -Set Up Functions
    
    private func setUpTemplate(){
        
        // search table
        searchTaskListTableView.delegate = self
        searchTaskListTableView.dataSource = self
        
        // search bar
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        // search bar color
        searchBar.barTintColor = UIColor.white
        searchBar.layer.borderColor = UIColor.white.cgColor
        searchBar.layer.borderWidth = 1
        
        // textfield in search bar color
        let textfield_search_controller = searchBar.value(forKey: "searchField") as! UITextField?
        textfield_search_controller?.backgroundColor = UIColor(rgb: SEARCH_BAR_COLOUR)
        
        // change search icon color
        let search_icon = textfield_search_controller?.leftView as! UIImageView
        search_icon.image = search_icon.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        search_icon.tintColor = UIColor(rgb: MERCK_GREEN)
        
        // add bottom line to table header
        codeBorder.borderColor = UIColor.darkGray.cgColor
        codeBorder.borderWidth = width
        codeBorder.frame = CGRect(x: 0, y: codeHeader.frame.size.height - width, width:  codeHeader.frame.size.width, height: codeHeader.frame.size.height)
        codeHeader.layer.addSublayer(codeBorder)
        codeHeader.layer.masksToBounds = true
        
        descriptionBorder.borderColor = UIColor.darkGray.cgColor
        descriptionBorder.borderWidth = width
        descriptionBorder.frame = CGRect(x: 0, y: descriptionHeader.frame.size.height - width, width:  descriptionHeader.frame.size.width, height: descriptionHeader.frame.size.height)
        descriptionHeader.layer.addSublayer(descriptionBorder)
        descriptionHeader.layer.masksToBounds = true
    }
<<<<<<< HEAD
    
    func setUpGD(){
        o.arrowColor = UIColor.red
        o.showBorder = false
        o.boxBackColor = UIColor.clear
        
        o.highlightView = true
        o.arrowWidth = 2.0
        o.backColor = UIColor.blue
        o.boxBorderColor = UIColor.black
        o.headColor = UIColor.white
        o.headRadius = 6
        o.labelFont = UIFont.systemFont(ofSize: 12)
        o.labelColor = UIColor.green
        
        // Currently only LineType.line_bubble and LineType.dash_bubble

        o.lineType = LineType.dash_bubble
        
        // If view controller has navigation bar, use it
        // to calculate the correct height
        //o.navHeight = o.calculateNavHeight(self)
        
        // Always set the delegate for SkipOverlayDelegate
        // for onSkipSignal() function call
        o.delegate = self
    }
    
    var a = 0
    func onSkipSignal(){
        a += 1
        // Skip each item here
        // Check sample project for more info on this
        switch (a){
        case 1:
            print (111)
            o.drawOverlay(self.view, containerWidth: 200, descText: "Filter tasks via code or description.", toView: searchBar, isCircle: false)
        case 2:
            o.drawOverlay(self.view, containerWidth: 200, descText: "Click on a task to edit hours.", toView:searchTaskListTableView, isCircle: false)
        default:
            a = 0
            o.removeFromSuperview()
        }
        
    }
    
    // create an instance of GDOverlay()
    var o = GDOverlay()

=======
>>>>>>> ea17c417d3b5b08e996f195d1f9f31bc701d6e89
}
