//
//  ViewController.swift
//  LAL
//
//  Created by Khor Le Yi on 12/6/17.
//  Copyright © 2017 Khor Le Yi. All rights reserved.
//

import UIKit

class Backup2: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate{
    
    //MARK: - Constants
    let SEARCH_BAR_COLOUR = 0xF2F2F2
    
    //MARK: - Variables
    let date_formatter = DateFormatter()
    let cell_identifier = "TaskTableViewCell"
    //let search_controller = NoCancelButtonSearchController(searchResultsController: nil)
    
    var display_date = Date()
    var display_day = 0
    var tasks = [Task]()
    var filtered_tasks = [Task]()
    var task_manager = TaskHelper()
    var hours_for_current_date = [String:String]()
    var total_hours = 0.0
    var is_searching = false
    
    //MARK: - Outlets
    @IBOutlet weak var monthBarView: UIView!
    @IBOutlet weak var dateMonthYearLabel: UILabel!
    @IBOutlet weak var taskListBackgroundView: UIView!
    @IBOutlet weak var taskListTableView: UITableView!
    @IBOutlet weak var totalHoursLabel: UILabel!
    @IBOutlet weak var profilePic: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: - Initialisation
    override func viewDidLoad() {
        super.viewDidLoad()
        display_day = Calendar.current.component(.day, from: display_date)
        task_manager.loadTaskDB()
        loadTaskDB()
        setUpTemplate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table View Functions
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if is_searching {
            return filtered_tasks.count
        }
        return task_manager.getNumberOfTasks()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cell_identifier, for: indexPath) as? DisplayTaskTableViewCell  else {
            fatalError("The dequeued cell is not an instance of TaskTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let task : Task
        if is_searching {
            task = filtered_tasks[indexPath.row]
        } else {
            task = tasks[indexPath.row]
        }
        cell.codeLabel.text = task.code
        cell.descriptionLabel.text = task.description
        cell.hoursLabel.text = String(format: "%.1f" , task.hours)
        return cell
    }
    
    private func loadTaskDB() {
        hours_for_current_date = task_manager.getTodayHours(date: display_day-1)
        tasks.removeAll()
        var task_description = ""
        for (key,value) in hours_for_current_date {
            task_description = task_manager.getTaskDescription(mCode: key)
            guard let task = Task(code : key, description : task_description, hours : Double(value)!) else {
                fatalError("Unable to instantiate task : " + key)
            }
            tasks += [task]
        }
    }
    
    //MARK: - Actions
    
    // called after saving hours
    @IBAction func updateHours(sender: UIStoryboardSegue) {
        guard let sourceViewController = sender.source as? DailyAddHoursViewController else {
            fatalError("Sender not DailyAddHoursViewController")
        }
        let passed_in_task = sourceViewController.task
        for (iteration,eachTask) in tasks.enumerated() {
            if eachTask.code == passed_in_task?.code {
                // update hours on table view
                tasks[iteration].hours = (passed_in_task?.hours)!
                taskListTableView.reloadRows(at: [IndexPath(row:iteration,section:0)] , with: .none)
                
                // deselect cell
                taskListTableView.deselectRow(at: IndexPath(row:iteration,section:0), animated: true)
                
                // update total hours
                setTotalHours()
                
                // update db
                task_manager.setHours(mCode: (passed_in_task?.code)!, day: display_day-1, hours: (passed_in_task?.hours)!)
                break
            }
        }
    }
    
    @IBAction func getNextDay(_ sender: UIButton) {
        addDay()
        loadTaskDB()
        taskListTableView.reloadData()
    }
    
    @IBAction func getPreviousDay(_ sender: UIButton) {
        subtractDay()
        loadTaskDB()
        taskListTableView.reloadData()
    }
    
    //MARK: -Navigation
    
    // This method lets you configure a view controller before it's presented.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "ShowDetail":
            guard let taskDetailViewController = segue.destination as? DailyAddHoursViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedTaskCell = sender as? DisplayTaskTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = taskListTableView.indexPath(for: selectedTaskCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selected_task : Task
            //            if search_controller.isActive && search_controller.searchBar.text != ""{
            //                selected_task = filtered_tasks[indexPath.row]
            //            } else {
            //                print ("else")
            selected_task = tasks[indexPath.row]
            //            }
            
            // disable search bar to load AddHoursViewController
            //            search_controller.isActive = false
            
            //let selectedMeal = tasks[indexPath.row]
            taskDetailViewController.task = selected_task
            
        case "SwitchToMonthlySegue":
            _ = 1
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
    
    // set transparency of AddHoursViewController to overlap over view controller
    func showModal() {
        let modalViewController = DailyAddHoursViewController()
        modalViewController.modalPresentationStyle = .overCurrentContext
        present(modalViewController, animated: true, completion: nil)
    }
    
    //MARK: -Search Bar Functions
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            is_searching = false
            view.endEditing(true)
            taskListTableView.reloadData()
        } else {
            is_searching = true
            filtered_tasks = tasks.filter { task in
                return task.code.lowercased().contains((searchBar.text?.lowercased())!)
            }
            taskListTableView.reloadData()
        }
    }
    
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        let textfield_search_controller = search_controller.searchBar.value(forKey: "searchField") as! UITextField?
    //        if touches.first != nil {
    //            textfield_search_controller?.resignFirstResponder()
    //        }
    //        super.touchesBegan(touches, with: event)
    //        search_controller.isActive = false
    //    }
    
    //    func filterContentForSearch(search_text: String, scope: String = "All"){
    //        filtered_tasks = tasks.filter { task in
    //            return task.code.lowercased().contains(search_text.lowercased())
    //        }
    //        taskListTableView.reloadData()
    //    }
    
    //MARK: -Set Up Functions
    
    private func setUpTemplate(){
        setTotalHours()
        
        // setting up look of calendar
        taskListBackgroundView.backgroundColor = .white
        taskListBackgroundView.layer.cornerRadius = 0.0
        var maskPath = UIBezierPath(roundedRect: taskListBackgroundView.bounds, byRoundingCorners: [.bottomLeft,.bottomRight], cornerRadii: CGSize(width: 10.0, height: 0.0))
        var maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        taskListBackgroundView.layer.mask = maskLayer
        
        monthBarView.layer.cornerRadius = 0.0
        maskPath = UIBezierPath(roundedRect: monthBarView.bounds, byRoundingCorners: [.topLeft,.topRight], cornerRadii: CGSize(width: 10.0, height: 0.0))
        maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        monthBarView.layer.mask = maskLayer
        
        // date
        date_formatter.dateStyle = .medium
        date_formatter.timeStyle = .none
        date_formatter.locale = Locale(identifier: "en_US")
        dateMonthYearLabel.text = date_formatter.string(from: display_date)
        
        // search bar
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        let frame = CGRect(x: 0, y: 0, width: 100, height: 10)
        let titleView = UIView(frame: frame)
        titleView.addSubview(searchBar)
        
        //        search_controller.searchResultsUpdater = self as? UISearchResultsUpdating
        //        search_controller.dimsBackgroundDuringPresentation = false
        //        definesPresentationContext = true
        //        taskListTableView.tableHeaderView = search_controller.searchBar
        //
        //        // search bar color
        //        search_controller.searchBar.barTintColor = UIColor.white
        //        search_controller.searchBar.layer.borderColor = UIColor.white.cgColor
        //        search_controller.searchBar.layer.borderWidth = 1
        //
        //        // textfield in search bar color
        //        let textfield_search_controller = search_controller.searchBar.value(forKey: "searchField") as! UITextField?
        //        textfield_search_controller?.backgroundColor = UIColor(rgb: SEARCH_BAR_COLOUR)
        //
        //        // change search icon color
        //        let search_icon = textfield_search_controller?.leftView as! UIImageView
        //        search_icon.image = search_icon.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        //        search_icon.tintColor = UIColor(rgb: MERCK_GREEN)
        
        // set up profile pic
        let nameArray = (UserDefaults.standard.string(forKey: USER_GIVENNAME) ?? "Hee").components(separatedBy: " ")
        var name_initial = ""
        for (index,name) in nameArray.enumerated(){
            if index > 1 {
                break
            }
            let first_char_index = name.index(name.startIndex,offsetBy:0)
            name_initial += String(describing: name[first_char_index])
        }
        profilePic.layer.cornerRadius = 20
        profilePic.layer.borderColor = UIColor.clear.cgColor
        profilePic.layer.borderWidth = 0.5
        profilePic.clipsToBounds = true
        profilePic.text = name_initial
    }
    
    //MARK: -Support functions
    func addDay() {
        display_date = display_date + TimeInterval(DAY_SECONDS)
        dateMonthYearLabel.text = date_formatter.string(from: display_date)
    }
    
    func subtractDay() {
        display_date = display_date - TimeInterval(DAY_SECONDS)
        dateMonthYearLabel.text = date_formatter.string(from: display_date)
    }
    
    func setTotalHours() {
        total_hours = 0.0
        for each_task in tasks{
            total_hours += Double(each_task.hours)
        }
        self.totalHoursLabel.text = String(format: "%.1f", total_hours) + " hrs"
    }
    
}
