//
//  DailyViewControllerTableView.swift
//  GTT
//
//  Created by Khor Le Yi on 28/6/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import Foundation
import UIKit

extension DailyViewController: UITableViewDelegate, UITableViewDataSource{
    
    //MARK: - Table View Functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if is_searching {
            return filtered_task_ids.count
        }
        return task_ids.count
    }
    
    // load data from task array into each cell
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cell_identifier, for: indexPath) as? DisplayTaskTableViewCell  else {
            fatalError("The dequeued cell is not an instance of TaskTableViewCell.")
        }
        // get table cell font size
        let font_size = cell.hoursLabel.font.pointSize
        
        // reset cell style (upon dequeue, cell style is not cleared)
        cell.hoursLabel.font = UIFont.systemFont(ofSize: font_size)
        cell.descriptionLabel.font = UIFont.systemFont(ofSize: font_size)
        cell.codeLabel.font = UIFont.systemFont(ofSize: font_size)
        
        // Fetches data for each cell
        let task_id : Int
        if is_searching {
            task_id = filtered_task_ids[indexPath.row]
        } else {
            task_id = task_ids[indexPath.row]
        }
        
        cell.codeLabel.text = String(describing: task_id)
        //cell.descriptionLabel.text = task_manager.getTaskName(taskID: task_id)
        cell.hoursLabel.text = String(format: "%.1f", Double(task_hours[String(task_id)]!)!)
        
        // APPEARANCE
        // bold cell if the hours not zero
        if Double(cell.hoursLabel.text!) != 0 {
            let font_size = cell.hoursLabel.font.pointSize
            cell.hoursLabel.font = UIFont.boldSystemFont(ofSize: font_size)
            cell.descriptionLabel.font = UIFont.boldSystemFont(ofSize: font_size)
            cell.codeLabel.font = UIFont.boldSystemFont(ofSize: font_size)
        }
        return cell
    }

}
