//
//  AddHoursViewController.swift
//  LAL
//
//  Created by Khor Le Yi on 12/6/17.
//  Copyright © 2017 Khor Le Yi. All rights reserved.
//

import UIKit
import os.log

class DailyAddHoursViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - Properties
    let increment_value = 0.1
    let increment_more_value = 1.0
    
    var input_value = 0.0
    var maximum_input_value = 24.0
    var minimum_input_value = 0.0
    var task : Task!                    // define task as part of the struct
    var hours : Double?
    var total_day_hours : Double?
    var display_date : Date?
    var timer = Timer()
    
    //MARK: - Outlets
    @IBOutlet weak var hourInput: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var addHourView: UIView!
    @IBOutlet weak var mcodeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    //MARK: - Initialisation
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpExistingTasks()
        setUpInterface()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        hourInput.becomeFirstResponder()
    }
    
    //MARK: -Setup Functions
    
    // Set up views if editing an existing Task
    func setUpExistingTasks(){
        // task != nil if it already exists
        guard let task = task else {
            fatalError("unable to find task")
        }
        
        // if task has no hours, input is nil
        if Double(task.hours) == minimum_input_value {
            self.hourInput.clearsOnBeginEditing = true
        } else {
            self.hourInput.text = String(format: "%.1f", (task.hours))
        }
    }
    
    // Set up interface
    func setUpInterface(){
        // show mcode and description
        mcodeLabel.text = String(describing:task.code)
        descriptionLabel.text = task.description
        descriptionLabel.sizeToFit()
        
        // numberPad as input keyboard
        hourInput.keyboardType = UIKeyboardType.decimalPad
        
        // handle the text field’s user input through delegate callbacks
        self.hourInput.delegate = self
        
        // set background to translucent
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        
        // set save button colour
        saveButton.backgroundColor = UIColor(rgb:MERCK_GREEN)
        
        // set maximum input value
        maximum_input_value = 24.0 - total_day_hours! + (task?.hours)!

    }
    
    
    
    //MARK: - Actions
    
    // close button
    @IBAction func closePopUp(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Navigation
    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any?) -> Bool {
        if let ident = identifier {
            if ident == "unwindToViewController" {
                // if value is larger than maximum_input, disable save
                
                if (hourInput.text?.isEmpty)! {
                    hourInput.text = String(format: "%.1f", 0.0)
                    return true
                } else if Double(hourInput.text!)! > maximum_input_value{
                    hourInput.text = String(format: "%.1f", maximum_input_value)
                    Drop.down("Total hours for today exceeded 24.0")
                    return false
                }
            }
        }
        return true
    }
    
    // This method lets you configure a view controller before it's presented.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        // Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIButton, button === saveButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        
        // Set the hours to be passed to DailyViewController after the unwind segue.
        task?.hours = Double(hourInput.text!)!
    }

    //MARK: - Delegates
    
    // limits number of "." entered
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate = self.hourInput.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        let arrayOfString = txtAfterUpdate.components(separatedBy: ".") as NSArray
        
        if arrayOfString.count > 2 {
            return false
        }
        return true
    }
    
    // change height of view to make save button on top of keypad
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0{
                let keyboard_height = keyboardSize.height
                let width = self.view.frame.width
                let height = self.view.frame.height
                let new_height = height - keyboard_height
                let new_y = self.view.frame.origin.y
                let x = self.view.frame.origin.x
                let newFrame = CGRect(x: x, y: new_y, width: width, height: new_height)
                
                self.view.frame = newFrame
            }
            
        }
        
    }
    
    
    // Sets value upon touching other places on screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if !(hourInput.text!.isEmpty) {
            input_value = Double(hourInput.text!)!;
            guard total_day_hours != nil else {
                fatalError("Total hours double does not exist")
            }
            let new_value = input_value
            inputValue(new_value: new_value)
        }
    }
    
    // Checks input value
    func inputValue(new_value : Double){
        // check that value entered by user does not exceed maximum input value
        if (new_value > maximum_input_value) {
            
            // notify user that value cannot exceed maximum_input_value
            Drop.down("Total hours for today exceeded 24.0")
            
            // set input text as maximum_input_value
            hourInput.text = String(format: "%.1f", maximum_input_value)
        } else if (new_value < minimum_input_value){
            hourInput.text = String(format: "%.1f", minimum_input_value)
        } else {
            hourInput.text = String(format: "%.1f", new_value)       // maximum 1 d.p
        }
    }
}
