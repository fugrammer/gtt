//
//  APIHandler.swift
//  GTT
//
//  Created by Thomas Tham on 30/6/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit

/// Typealias for API webservice handler
typealias ApiWebserviceHandler = (_ json: AnyObject? , _ error : NSError? , _ alertMessage: Bool ,_ statusMessage: String)  -> Void


class APIHandler: NSObject, URLSessionDelegate {
    
    lazy var conn: URLSession = {
        let config = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        return session
    }()
    
    /// Web service call
    ///
    /// - parameter url:        url to make a service call
    /// - parameter parameters: parameter value
    /// - parameter headers:    content-type,accept-type and auth-token
    /// - parameter method:     GET,POST,PUT,....
    /// - parameter handler:    ApiWebserviceHandler
    func callWebService(url: String, parameters: Data,headers: [String: String]?,method:String, handler:@escaping ApiWebserviceHandler) {
        
        let url:NSURL = NSURL(string: url)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = method
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        if(method == "GET") {
            request.allHTTPHeaderFields = headers
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                guard let responseData = data else {
                    print("GET - Error: did not receive data")
                    return
                }
                guard error == nil else {
                    print("error calling GET\(String(describing: error?.localizedDescription))")
                    return
                }
                handler(responseData as AnyObject?, nil, true, "")
            })
            task.resume()
        } else if (method == "POST") {
            
            request.httpBody = parameters
            request.allHTTPHeaderFields = headers
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                guard let responseData = data else {
                    print("POST - Error: did not receive data")
                    handler(nil, nil,false, "Error: did not receive data")
                    return
                }
                guard error == nil else {
                    print("error calling POST")
                    handler(nil, nil,false, "Error calling POST : \(String(describing: error?.localizedDescription))")
                    return
                }
                
                guard let validResponse = response as? HTTPURLResponse else {
                    print("invalid POST response")
                    handler(nil, nil,false, "Invalid response")
                    return
                }
                
                let statusCode = validResponse.statusCode
                
                switch statusCode {
                // Server returned not authorized
                case 401:
                    print("POST - Error, not authorized")
                    handler(nil, nil,false, "Not Authorized")
                    return
                case 500:
                    handler(nil, nil,false, "Something went wrong.Please try again later")
                    return
                case 200:
                    handler(responseData as AnyObject?, nil,true, "Success")
                    return
                default:
                    print("POST - Network failure! statusCode: \(statusCode), response: \(String(describing: response))")
                    
                    // Try to parse the data response - which gives more hint on what's wrong with the API
                    
                    DispatchQueue.main.async {
                        
                        let responseDict: NSDictionary?
                        do {
                            responseDict = try JSONSerialization.jsonObject(with: (responseData as NSData) as Data,
                                                                            options: []) as? NSDictionary
                            
                            guard let responseDict = responseDict else {
                                return
                            }
                            print("POSt - response dict\(responseDict)")
                            handler(responseData as AnyObject?, nil, false , responseDict.object(forKey: "message") as! String)
                        } catch  {
                            print("POST - error trying to convert data to JSON")
                            if let data = data, let parsedData = try? NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil) {
                                print("POST - Parsed data for failed response: \(parsedData.string) ")
                                handler(nil, nil, false , parsedData.string)
                            }
                            return
                        }
                    }
                }
            })
            task.resume()
        }
    }
    
    func getProfilePicture() {
        
        var header = [String: String]()
        header["Content-Type"] = "image/jpeg"
        header["X-Merck-APIKey"] = CLIENT_ID
        
        let urlString = BASE_URL_INTERNAL_DIR+"/users/"+UserDefaults.standard.string(forKey: USER_ISID)!+"/photo"
        
        let url:NSURL = NSURL(string: urlString)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url as URL)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        request.allHTTPHeaderFields = header
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            guard let responseData = data else {
                print("GET - Error: did not receive data")
                return
            }
            guard error == nil else {
                print("error calling GET\(String(describing: error?.localizedDescription))")
                return
            }
            UserDefaults.standard.set(responseData, forKey: "ProfileImage")
            //handler(responseData as AnyObject?, nil, true, "")
        })
        task.resume()
    }
}
