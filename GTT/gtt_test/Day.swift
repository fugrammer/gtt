//
//  Day.swift
//  gtt_test
//
//  Created by KHOR, LE YI on 6/10/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import UIKit

class CalendarDay{
    
    //MARK: Properties
    var date : Int
    var hours : Float?
    
    init?(date:Int,hours:Float){
        guard (date>=1) && (date<=31) else{
            return nil
        }
        self.date = date
        guard (hours>=0) && (hours<=24) else{
            return nil
        }
        self.hours = hours
    }
}
