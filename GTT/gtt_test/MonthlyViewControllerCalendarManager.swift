//
//  MonthlyViewControllerCalendarManager.swift
//  GTT
//
//  Created by THAM, THOMAS on 6/22/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit
import JTAppleCalendar
import NVActivityIndicatorView


extension MonthlyViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource{
    
    //MARK: - Calendar delegates
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MonthlyCalendarCollectionViewCell
       
        let year = self.calendar.component(.year, from: cellState.date)
        let month = self.calendar.component(.month, from: cellState.date)
        let day = self.calendar.component(.day, from: cellState.date)
        
        let date = DateHelper.dateFromComponents(year: year , month: month, day: day)
        if let day = MonthlyViewController.all_days[date]{
            // Calendar day is in open period
            cell.enabled = day.enabled
            if cellState.dateBelongsTo == .thisMonth{
                cell.dateLabel.text = cellState.text
                cell.hoursLabel.text = String(describing: day.hours)
                cell.hoursLabel.textColor = UIColor(rgb:DAY_TEXT_UNSELECTED_COLOR)
                if cell.enabled == false{
                    cell.dateLabel.textColor = UIColor(rgb: DAY_TEXT_UNSELECTED_COLOR)
                    cell.backgroundImageView.image = #imageLiteral(resourceName: "disabled_day")
                    
                } else if cellState.isSelected == false{
                    if cellState.day.rawValue == 1 || cellState.day.rawValue == 7{
                        cell.backgroundImageView.image = #imageLiteral(resourceName: "weekend")
                    } else {
                        cell.backgroundImageView.image = #imageLiteral(resourceName: "weekday")
                    }
                } else {
                    cell.dateLabel.textColor = UIColor(rgb:DAY_TEXT_SELECTED_COLOR)
                    cell.backgroundImageView.image = #imageLiteral(resourceName: "selected_day")
                }
                
                cell.isHidden = false
            } else {
                // Hide leading and trailing days for prev/next months
                cell.isHidden = true
            }
        }
            
        else {
            // Calendar day is not in open period
            cell.enabled = false
            if cellState.dateBelongsTo == .thisMonth{
                cell.dateLabel.text = cellState.text
                cell.hoursLabel.text = "--"
                cell.hoursLabel.textColor = UIColor(rgb:DAY_TEXT_UNSELECTED_COLOR)
                cell.dateLabel.textColor = UIColor(rgb: DAY_TEXT_UNSELECTED_COLOR)
                cell.backgroundImageView.image = #imageLiteral(resourceName: "disabled_day")
                cell.isHidden = false
            } else {
                cell.isHidden = true
            }
        }
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        guard let cell = cell as? MonthlyCalendarCollectionViewCell else {
            fatalError()
        }
        
        let cellIndex = (Int(cellState.text) ?? 0) - 1
        guard cellIndex >= 0 && cellIndex <= 30 else {
            fatalError()
        }
        // Check if day is in open period
        if let day = MonthlyViewController.all_days[date]{
            if day.enabled == false{
                // Cell selected is not enabled
                cell.isSelected = false
                return
            } else if day_selected == nil{
                // No cell was previously selected
                day.selected = true
                day_selected = date
                selectCell(cell, cellState)
            } else if day_selected == date{
                // User clicked on active cell
                day.selected = false
                day_selected = nil
                deselectCell(cell, cellState)
                cell.isSelected = false
            }
            else{
                // User clicked on an inactive cell
                MonthlyViewController.all_days[day_selected!]?.selected = false
                day.selected = true
                selectCell(cell,cellState)
                day_selected = date
            }

        }else{
            cell.isSelected = false
            return
        }
        loadTaskTable(day_selected)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        guard let cell = cell as? MonthlyCalendarCollectionViewCell else {
            return
        }
        deselectCell(cell,cellState)
    }
    
    // Update calendar view when user changed month
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
      
        let date = visibleDates.monthDates[0].date
        
        sectionNumber = visibleDates.monthDates[0].indexPath[0]
        rightButton.isEnabled = true
        leftButton.isEnabled = true
        
        if sectionNumber == calendarCollectionView.numberOfSections-1{
            rightButton.isEnabled = false
        }
        if sectionNumber == 0 {
            leftButton.isEnabled = false
        }
        
        CURRENT_YEAR = self.calendar.component(.year, from: date)
        CURRENT_MONTH = self.calendar.component(.month, from: date)
        day_selected = nil
        
        calendar.deselectAllDates()
        
        setUpCalendar()
        
        DispatchQueue.main.async {
            // Update UI
            self.loadTaskTable(nil)
            self.totalHoursLabel.text = "\(String(format: "%.1f", self.months_total_hours[self.CURRENT_MONTH]!)) hrs"
            self.calendarCollectionView.reloadData()
            self.stopAnimating()
            if let _message = self.message{
                Drop.down(_message, duration: 1.5)
                self.message = nil
            }
            // Check if tutorial should be launched
            let tutorial = UserDefaults.standard.bool(forKey: "TUTORIAL")
//            tutorial = false
            if tutorial == false{
                UserDefaults.standard.set(true,forKey:"TUTORIAL")
                self.presentTutorial()
            }
        }
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        
    
        taskManager.setLocalDB()
        let start_end_dates = taskManager.getOpenDates()
        START_DATE = start_end_dates["startDate"]
        END_DATE = start_end_dates["endDate"]
    
        self.calendar.timeZone = TimeZone(abbreviation: "GMT")!
        
        let startDate = START_DATE!
        let endDate = END_DATE!
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: 6, // Only 1, 2, 3, & 6 are allowed
            calendar: self.calendar,
            generateInDates: .forAllMonths,
            generateOutDates: .tillEndOfGrid,
            firstDayOfWeek: .sunday)
        return parameters
    }
    
    /// Revert cell to unselected state
    private func deselectCell(_ cell : JTAppleCell,_ cellState: CellState){
        guard let cell = cell as? MonthlyCalendarCollectionViewCell else {
            return
        }
        
        cell.dateLabel.textColor = UIColor(rgb: DAY_TEXT_UNSELECTED_COLOR)
        
        if cell.enabled == false {
            cell.backgroundImageView.image = #imageLiteral(resourceName: "disabled_day")
            return
        }
        
        switch (cellState.day.rawValue){
        case 1, 7:
            cell.backgroundImageView.image = #imageLiteral(resourceName: "weekend")
        default:
            cell.backgroundImageView.image = #imageLiteral(resourceName: "weekday")
        }
    }
    
    /// Change cell to selected state
    private func selectCell(_ cell : JTAppleCell, _ cellState:CellState){
        guard let cell = cell as? MonthlyCalendarCollectionViewCell else {
            return
        }
        
        cell.dateLabel.textColor = UIColor(rgb: DAY_TEXT_SELECTED_COLOR)
        cell.backgroundImageView.image = #imageLiteral(resourceName: "selected_day")
    }
    
    
    
}
