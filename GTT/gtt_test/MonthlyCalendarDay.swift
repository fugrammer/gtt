//
//  Day.swift
//  gtt_test
//
//  Created by THAM, THOMAS on 6/10/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit

/// Class for individual collection view cell for calendar
class MonthlyCalendarDay{
    
    //MARK: - Constants
    let UNSELECTED = "unselected"
    let SELECTED = "selected"
    let DISABLED = "disabled"
    
    //MARK: - Properties
    var date : Int
    var hours : Double
    var state : String
    var day : Int
    var selected : Bool
    var date_object : Date
    var enabled : Bool
    
    //MARK: - Initialisation
    init(date:Int,hours:Double,day:Int, enabled: Bool){
        guard (date>=1) && (date<=31) else{
            fatalError()
        }
        guard (hours>=0) && (hours<=24) else{
            fatalError()
        }
        
        self.date = date
        self.hours = hours
        self.state = UNSELECTED
        self.day = day
        self.selected = false
        self.date_object = Date()
        self.enabled = enabled
    }
    
    init(date:Int,hours:Double,day:Int, enabled: Bool, date_object: Date){
        guard (date>=1) && (date<=31) else{
            fatalError()
        }
        guard (hours>=0) && (hours<=24) else{
            fatalError()
        }
        
        self.date = date
        self.hours = hours
        self.state = UNSELECTED
        self.day = day
        self.selected = false
        self.date_object = date_object
        self.enabled = enabled
    }
    
    //MARK: - Public Methods
    
    /// Update hours for cell
    func updateHours (hours:Double){
        self.hours = hours
    }
}
