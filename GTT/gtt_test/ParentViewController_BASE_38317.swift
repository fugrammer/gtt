//
//  ParentViewController.swift
//  GTT
//
//  Created by Khor Le Yi on 12/7/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import UIKit

class ParentViewController: UIViewController {

    enum TabIndex : Int {
        case firstChildTab = 0
        case secondChildTab = 1
    }
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var profileName: UILabel!
    
    var currentViewController: UIViewController?
    lazy var firstChildTabVC: UIViewController? = {
        let firstChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "MonthlyStoryboard")
        return firstChildTabVC
    }()
    lazy var secondChildTabVC : UIViewController? = {
        let secondChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "DailyView")
        
        return secondChildTabVC
    }()

    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.selectedSegmentIndex = TabIndex.firstChildTab.rawValue
        displayCurrentTab(TabIndex.firstChildTab.rawValue)
        
        // Setup profile name
        let nameArray = (UserDefaults.standard.string(forKey: USER_GIVENNAME) ?? "Hee").components(separatedBy: " ")
        var name_initial = ""
        for (index,name) in nameArray.enumerated(){
            if index > 1 {
                break
            }
            let first_char_index = name.index(name.startIndex,offsetBy:0)
            name_initial += String(describing: name[first_char_index])
        }
        profileName.layer.cornerRadius = 20
        profileName.layer.borderColor = UIColor.clear.cgColor
        profileName.layer.borderWidth = 0.5
        profileName.clipsToBounds = true
        profileName.text = name_initial
        
        // set up toggle
        segmentedControl.tintColor = UIColor(rgb: MERCK_GREEN)
        segmentedControl.backgroundColor = UIColor.white
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
    }
    
    // MARK: - Switching Tabs Functions
    @IBAction func switchTabs(_ sender: UISegmentedControl) {
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParentViewController()
        
        displayCurrentTab(sender.selectedSegmentIndex)
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            
            vc.view.frame = self.contentView.bounds
            self.contentView.addSubview(vc.view)
            self.currentViewController = vc
        }
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.firstChildTab.rawValue :
            vc = firstChildTabVC
        case TabIndex.secondChildTab.rawValue :
            vc = secondChildTabVC
        default:
            return nil
        }
        
        return vc
    }
}
