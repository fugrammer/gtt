
//
//  MonthlyEditViewControllerCalendarManager.swift
//  GTT
//
//  Created by THAM, THOMAS on 6/22/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit
import JTAppleCalendar

extension MonthlyEditViewController:JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource{
    
    //MARK: - Calendar
    
    // Present calendar cells information - Date, Hours, Enable state, etc.
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CalendarCellView", for: indexPath) as! MonthlyCalendarCollectionViewCell
        
        guard let temp = Int(cellState.text) else{
            fatalError()
        }
        
        cell.enabled = taskManager.isDayOpen(date: cellState.date)
 
        if cellState.dateBelongsTo == .thisMonth{
            let day = days[temp-1]
            cell.dateLabel.text = cellState.text
            cell.hoursLabel.text = String(describing: day.hours)
            cell.hoursLabel.textColor = UIColor(rgb:DAY_TEXT_UNSELECTED_COLOR)
            
            if cell.enabled == false{
                cell.dateLabel.textColor = UIColor(rgb: DAY_TEXT_UNSELECTED_COLOR)
                cell.backgroundImageView.image = #imageLiteral(resourceName: "disabled_day")
            }else
                if day.selected == false{
                    cell.dateLabel.textColor = UIColor(rgb:DAY_TEXT_UNSELECTED_COLOR)
                    if cellState.day.rawValue == 1 || cellState.day.rawValue == 7{
                        cell.backgroundImageView.image = #imageLiteral(resourceName: "weekend")
                    } else {
                        cell.backgroundImageView.image = #imageLiteral(resourceName: "weekday")
                    }
                } else {
                    cell.dateLabel.textColor = UIColor(rgb:DAY_TEXT_SELECTED_COLOR)
                    cell.backgroundImageView.image = #imageLiteral(resourceName: "selected_day")
            }
            cell.isHidden = false
        }
        else {
            cell.isHidden = true
        }
        return cell
    }
    
    // Tracks days selected by user
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        guard let cell = cell as? MonthlyCalendarCollectionViewCell else {
            fatalError()
        }
        
        let cellIndex = (Int(cellState.text) ?? 0) - 1
        guard cellIndex >= 0 && cellIndex <= 30 else {
            fatalError()
        }
        
        if days[cellIndex].enabled == false{
            cell.isSelected = false
            return
        }
        
        days[cellIndex].selected = true
        numberDaysSelected += 1
        selectCell(cell, cellState)
        if numberDaysSelected>0{
            switchToDeselectAll()
            var minimumSelectedHours = 24.0
            for day in days{
                if day.selected == true{
                    minimumSelectedHours = min(minimumSelectedHours, day.hours)
                }
            }
            editHoursLabel.text = String(minimumSelectedHours)
        } else{
            switchToSelectAll()
            editHoursLabel.text = "0.0"
        }
    }
    
    // Unselect days
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        guard let cell = cell as? MonthlyCalendarCollectionViewCell else {
            return
        }
        let cellIndex = (Int(cellState.text) ?? 0) - 1
        guard cellIndex >= 0 && cellIndex <= 30 else {
            fatalError()
        }
        days[cellIndex].selected = false
        deselectCell(cell,cellState)
        if numberDaysSelected>0{
            switchToDeselectAll()
            var minimumSelectedHours = 24.0
            for day in days{
                if day.selected == true{
                    minimumSelectedHours = min(minimumSelectedHours, day.hours)
                }
            }
            editHoursLabel.text = String(minimumSelectedHours)
        } else{
            switchToSelectAll()
            editHoursLabel.text = "0.0"
        }
    }
    
    // Set up calendar
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        var startDate : Date
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = self.calendar.timeZone
        formatter.locale = self.calendar.locale
        
        if let temp = formatter.date(from: "\(CURRENT_YEAR!) \(CURRENT_MONTH!) 01"){
            startDate = temp
        } else {
            startDate = Date()
        }
        let endDate = startDate                                // You can also use dates created from this function
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: 6, // Only 1, 2, 3, & 6 are allowed
            calendar: self.calendar,
            generateInDates: .forAllMonths,
            generateOutDates: .tillEndOfGrid,
            firstDayOfWeek: .sunday)
        
        return parameters
    }

}
