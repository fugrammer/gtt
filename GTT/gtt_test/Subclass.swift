//
//  Subclass.swift
//  GTT
//
//  Created by Khor Le Yi on 18/6/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class TopAlignedLabel: UILabel {
//    let line_spacing = 0
    let y_offset = CGFloat(0)
    
    override func drawText(in rect: CGRect) {
        if let stringText = text {
            let stringTextAsNSString = stringText as NSString
//            let textStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
//            textStyle.lineSpacing = CGFloat(line_spacing)
            let textFontAttributes = [
                NSFontAttributeName: font
//                NSParagraphStyleAttributeName: textStyle
            ] as [String : Any]
            let labelStringSize = stringTextAsNSString.boundingRect(with: CGSize(width: self.frame.width,height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: textFontAttributes, context: nil).size
            super.drawText(in: CGRect(x:0,y: y_offset,width: self.frame.width, height:ceil(labelStringSize.height)))
        } else {
            super.drawText(in: rect)
        }
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        layer.borderWidth = 1
        layer.borderColor = UIColor.black.cgColor
    }
}
