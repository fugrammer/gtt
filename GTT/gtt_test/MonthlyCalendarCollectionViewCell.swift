//
//  CalendarCollectionViewCell.swift
//  gtt_test
//
//  Created by THAM, THOMAS on 6/9/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit
import JTAppleCalendar

class MonthlyCalendarCollectionViewCell: JTAppleCell {
    
    //MARK: Properties
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var enabled : Bool?
    
}
