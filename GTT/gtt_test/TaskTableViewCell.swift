//
//  TaskTableViewCell.swift
//  gtt_test
//
//  Created by KHOR, LE YI on 6/14/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    
    //MARK: - Outlets

    @IBOutlet weak var taskCode: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
