//
//  MonthlyViewControllerTaskManager.swift
//  GTT
//
//  Created by THAM, THOMAS on 6/22/17.
//  Copyright © 2017 Merck. All rights reserved.
//

import UIKit
import JTAppleCalendar

extension MonthlyViewController: UITableViewDelegate, UITableViewDataSource{
    
    //MARK: - Task table delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tasks.count > 0
        {
            tableView.separatorStyle = .singleLine
            tableView.backgroundColor = UIColor(rgb: 0xFFFFFF)
            tableView.backgroundView = nil
        }
        else
        {
            // Show "No task added" on tableview
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No task entered"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.backgroundColor = UIColor(rgb: CALENDAR_BACKGROUND_COLOR)
            tableView.separatorStyle  = .none
        }
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DisplayTaskTableViewCell else{
            fatalError("The dequeued cell is not an instance of DisplayTaskTableViewCell")
        }
        
        let task = tasks[indexPath.row]
        cell.codeLabel.text = String(describing: task.code)
        cell.descriptionLabel.text = task.description
        cell.hoursLabel.text = String(describing: task.hours)
        
        return cell
    }
    
    @objc private func revertColor(){
        taskTableView.reloadData()
    }
    
    // Load tasks and display them.
    func loadTaskTable(_ day : Date?){
        tasks = []
        let current_date = DateHelper.dateFromComponents(year: CURRENT_YEAR, month: CURRENT_MONTH, day: 2)
        for taskID in self.taskManager.getAllTaskIds(date:current_date){
            let task_description = taskManager.getTaskName(date: current_date, taskID: taskID)
            if day != nil{
                let task_hour = taskManager.getDateHours(date: day!, taskID: taskID)
                if Double(task_hour) == 0{
                    continue
                }
                tasks.append(Task(code:taskID, description: task_description, hours: Double(task_hour)!)!)
            } else {
                let task_hours = taskManager.getMonthHours(date: current_date, taskID: taskID)
                var sum = 0.0
                for hour in task_hours{
                    sum += Double(hour)!
                }
                if sum == 0{
                    continue
                }
                tasks.append(Task(code:taskID, description: task_description, hours: sum)!)
            }
        }
        taskTableView.reloadData()
    }
}
