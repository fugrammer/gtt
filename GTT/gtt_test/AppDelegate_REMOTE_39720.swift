//
//  AppDelegate.swift
//  gtt_test
//
//  Created by KHOR, LE YI on 6/9/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var firstEntry = true

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        guard userLoggedIn() == true else {
            //showOAuthLogin()
            return true
        }
        //showLogin()
        //showOAuthLogin()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated late    r.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        firstEntry = false
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        // Go to login back
        print (firstEntry)
        //self.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginOAuthViewController = mainStoryboard.instantiateViewController(withIdentifier: "MonthlyEditStoryboard") as! MonthlyEditViewController
        self.window?.rootViewController = loginOAuthViewController
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        UserDefaults.standard.set(false, forKey: USER_LOGGED_IN)
    }
    
    //MARK: Private methods
    
    private func showOAuthLogin(){
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginOAuthViewController = mainStoryboard.instantiateViewController(withIdentifier: STORYBOARD_ID_OAUTH) as! OAuthViewController
        self.window?.rootViewController = loginOAuthViewController
    }
    
    private func userLoggedIn() -> Bool{
        return UserDefaults.standard.bool(forKey: USER_LOGGED_IN)
    }
}

