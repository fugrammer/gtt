//
//  DailyViewControllerNavigationBar.swift
//  GTT
//
//  Created by Khor Le Yi on 28/6/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import Foundation
import UIKit

extension DailyViewController {
    //MARK: -Previous and Next Day
    func setUpNavigationBar(){
        // set actions for previous next day button
        previousDayButton.addTarget(self, action:#selector(self.getPreviousDay), for: .touchUpInside)
        nextDayButton.addTarget(self, action:#selector(self.getNextDay), for: .touchUpInside)
        addGesture()
    }
    
    // activated by minus button
    func getPreviousDay(button: UIButton) {
        if subtractDay() {
            endSearch()
            sortTasks()
            setTotalHours()
            taskListTableView.reloadData()
        }
    }
    
    // activated by add button
    func getNextDay(button: UIButton) {
        if addDay() {
            endSearch()
            sortTasks()
            setTotalHours()
            taskListTableView.reloadData()
        }
        
    }
    
    // add left right gestures
    func addGesture() {
        let swipe_right = UISwipeGestureRecognizer(target: self, action: #selector(DailyViewController.swiped(_gesture:)))
        swipe_right.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipe_right)
        
        let swipe_left = UISwipeGestureRecognizer(target: self, action: #selector(DailyViewController.swiped(_gesture:)))
        swipe_left.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipe_left)
    }
    
    func swiped(_gesture: UISwipeGestureRecognizer) {
        
        switch _gesture.direction {
        // view previous day
        case UISwipeGestureRecognizerDirection.right:
            if subtractDay() {
                endSearch()
                sortTasks()
                setTotalHours()
                taskListTableView.reloadData()
            }
        
        // view next day
        case UISwipeGestureRecognizerDirection.left:
            if addDay() {
                endSearch()
                sortTasks()
                setTotalHours()
                taskListTableView.reloadData()
            }
            
        default:
            print("other swipe")
        }
    }
    
    func addDay() -> Bool {
        // check if next day is open
        if task_manager.isDayOpen(date: (display_date + TimeInterval(DAY_SECONDS))) {
            display_date = display_date + TimeInterval(DAY_SECONDS)
            dateMonthYearLabel.text = date_formatter.string(from: display_date)
            return true
        } else {
            Drop.down("The next day is unavailable for editing")
            return false
        }
    }
    
    func subtractDay() -> Bool {
        // check if next day is open
        if task_manager.isDayOpen(date: (display_date - TimeInterval(DAY_SECONDS))) {
            display_date = display_date - TimeInterval(DAY_SECONDS)
            dateMonthYearLabel.text = date_formatter.string(from: display_date)
            return true
        } else {
            Drop.down("The previous day is unavailable for editing")
            return false
        }
    }
}
