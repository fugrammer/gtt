//
//  Task.swift
//  gtt_test
//
//  Created by KHOR, LE YI on 6/14/17.
//  Copyright © 2017 KHOR, LE YI. All rights reserved.
//

import Foundation
import UIKit

class SearchTask{
    
    //MARK: Properties
    
    var code: Int
    var description: String
    
    init?(code: Int, description: String) {
        // The name must not be empty
        guard !description.isEmpty else {
            return nil
        }
        
        // Initialize stored properties.
        self.code = code
        self.description = description
    }
    
}
